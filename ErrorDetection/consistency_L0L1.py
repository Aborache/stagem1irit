'''
This script is used to detect the list & number of files that exist in 
DEV_DESCRIPTORS_L0 but not in DEV_DESCRIPTORS_L1 and vice versa
'''

import os
import Resources.paths as paths

in_path_l0 = paths.__PATH_TO_DEV_DESCRIPTORS_L0__
#in_path_l1 = paths.__PATH_TO_DEV_DESCRIPTORS_L1__
in_path_l1 = "/home/hassan/6-2017/l1- only one level/"
'''
Used os.walk("path") that returns a type generator(tuple of 3 elements):
    dir_path, subdir_list, file_list = os.walk(in_path)
    
We use index 2 that contains the file_list
'''
#Read all files names in path L1 and store them in sorted list_L1
l1_generator = os.walk(in_path_l1)
list_l1 = l1_generator.next()[2]
#print len(list_l1)
list_l1.sort()
l1_generator.close()

#Read all files names in path L0 and store them in sorted list_L0
l0_generator = os.walk(in_path_l0)
list_l0 = l0_generator.next()[2]
#print len(list_l1)
list_l0.sort()
l0_generator.close()
    
#Detect the names and number of files in L0 but not in L1 and store them in
#local file files_L0_not_L1.txt
file_l0_not_l1 = open("files_L0_not_L1.txt", "w")
count_L0_not_L1 = 0    
for fname in list_l0:
    if fname not in list_l1:
        count_L0_not_L1 += 1
        print(fname)
        file_l0_not_l1.write(fname + "\n")
print("----------------------\nFiles in L0 but not in L1 : ", count_L0_not_L1,"\n----------------------")
file_l0_not_l1.close()

#Detect the names and number of files in L1 but not in L0 and store them in
#local file files_L1_not_L0.txt
file_l1_not_l0 = open("files_L1_not_L0.txt", "w")
count_L1_not_L0 = 0   
for fname in list_l1:
    if fname not in list_l0:
        count_L1_not_L0 += 1
        print(fname)
        file_l1_not_l0.write(fname + "\n")
print("----------------------\nFiles in L1 but not in L0 : ", count_L1_not_L0,"\n----------------------")
file_l1_not_l0.close()