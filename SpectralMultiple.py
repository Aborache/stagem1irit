# -*- coding: utf-8 -*-
"""
Created on Thu Jun 20 11:03:35 2019

@author: Lenovo T420s
"""

from sklearn.cluster import SpectralClustering
from multiprocessing import Process, Queue
import json
import numpy as np
from Resources import paths
from datetime import datetime
import matplotlib.pyplot as plt
import math
from collections import deque

def load_file():

    save_united = str(paths.__temp_file_video_to_united)
    fu = open(str(save_united),'r')
    
    u = json.load(fu)
    print(u['1801Media-iPhone3GWhatDoPeopleKnow259'].keys())
    return u

def separator(united):
    context = {}
    multi_context = {}
    name_truth = {}
    name_word = {}
    name_words = {}
    
    for  n, obj in united.items():
        context[n] = obj['meanVector']
        multi_context[n] = obj['KmeanVectors']
        name_truth[n] = obj['truth']
        name_word[n] = obj['meanWord']
        name_words[n] = obj['words']
    return (context,multi_context,name_truth,name_word,name_words)

def one_Kmean(vector_list,nb_Clusters,q):
    print("start",nb_Clusters)
    sc = SpectralClustering(n_clusters=nb_Clusters, random_state=0).fit(vector_list)
    labels = sc.labels_
    centers = {}
    for e in range (0, len(nb_Clusters)):
        centers[e] = []
    
    for e in range (0, len(vector_list)):
        centers[labels[e]].append(vector_list[e])
    
    for e in range (0, len(nb_Clusters)):
        centers[e] = np.mean(centers[e],axis = 0)   
        
    centers = sc.cluster_centers_
    inter_Class  = np.sum([np.linalg.norm(centers[i] - vector_list[j])
        for i in range (0 ,len(centers))
        for j in range (0, vector_list.shape[0]) if i != labels[j]])
    
    print("computation end",nb_Clusters)
    q.put(labels,sc.inertia_ , inter_Class,nb_Clusters)
    print("proc end ",nb_Clusters)
    return 1

def one_KmeanSimple(vector_list,nb_Clusters):
    sc = SpectralClustering(n_clusters=nb_Clusters, random_state=0,).fit(vector_list)
    labels = sc.labels_
    centers = {}
    for e in range (0, nb_Clusters):
        centers[e] = []
    
    for e in range (0, len(vector_list)):
        centers[labels[e]].append(vector_list[e])
    
    for e in range (0, nb_Clusters):        
        centers[e] = np.mean(centers[e],axis = 0)
        
    inter_Class  = np.average([np.linalg.norm(centers[i] - vector_list[j])
        for i in range (0 ,len(centers))
        for j in range (0, vector_list.shape[0]) if i != labels[j]])
    
    intra_class = np.average([np.linalg.norm(centers[i] - vector_list[j])
        for i in range (0 ,len(centers))
        for j in range (0, vector_list.shape[0]) if i == labels[j]])

    return (labels,intra_class , inter_Class)

def one_step(video_vect):
    nb = len(video_vect)
    context = np.array(list(video_vect.values()))
    result = Queue()
    proc= []
    for i in range (2, int(nb/3)):
        print("launch",i)
        p = Process(target=one_Kmean, args=(context,i,result,))
        p.start()
        proc.append(p)
    
    
    l,inert,inter_c,nbc = result.get()
    
    for i in range (1, len(proc)):
        print("reading",i)
        lb,ib,cb,nb = result.get()
        if (ib/cb) < (inert/inter_c):
            l = lb
            inert = ib
            inter_c = cb
            nbc = nb
    
    cpt = 0
    clusters = {}
    for i in (0,nbc):
        clusters[i] = {}
        
    for name,vec in video_vect.items():
        clusters[l[cpt]][name] = vec
        cpt += 1
        
    print("join")
    cpt = 0
    while(len(proc)> 0):
        proc.pop()
        cpt +=1
        print(cpt," have join")
        
    return(clusters,inert,inter_c)

def one_stepSimple(video_vect):
    nb = len(video_vect)
    context = np.array(list(video_vect.values()))
  
    l,inert,inter_c = one_KmeanSimple(context,2)
    nbc = 2
    nodes = [2]
    inertias = [inert]
    inter_class = [inter_c]
    score_evolution = [inert/inter_c]
    
    reduction = False
    stop = False
    nb_node = 3 
    while((not stop) and nb_node < int(math.sqrt(nb))):
        lb,ib,cb = one_KmeanSimple(context,nb_node)
        
        nodes.append(nb_node)
        inertias.append(ib)
        inter_class.append(cb)
        score_evolution.append(ib/cb)
        if (ib/cb) < (inert/inter_c):
            l = lb
            inert = ib
            inter_c = cb
            nbc = nb_node
            reduction = True
        elif reduction:
            stop = True
        
        nb_node += 1    
    
    """
    plt.figure(1)
    plt.subplot(311)
    plt.plot(nodes, inertias, 'r--')
    plt.subplot(312)
    plt.plot( nodes,inter_class, 'bs')
    plt.subplot(313)
    plt.plot(nodes, score_evolution, 'g^')
    plt.show()
    
    
    print(l,inert,inter_c,nbc)
    """
    cpt = 0
    clusters = {}
    for i in range (0,nbc):
        clusters[i] = {}
        
    for name,vec in video_vect.items():
        clusters[l[cpt]][name] = vec
        cpt += 1
    return(clusters,inert,inter_c)

def recurs_clustering(video_vect,t,w,step,score):
    if step == 1 or len(video_vect)<9 :
        return ([[k,t[k],w[k]] for k in video_vect.keys()])
    res,inert,inter = one_step(video_vect)
    print(inert/inter)
    if  score <= (inert/inter):
        return [[k,t[k],w[k]] for k in video_vect.keys() ]
    else:
        return([recurs_clustering(res[k],t,w,step-1,(inert/inter)) for k in res.kes()])
 
def convert_to_read_(videos,truth,words,simple = True):
    presentation = {}
    presentation["videos"] = videos
    presentation["words"] = []
    presentation["truths"] = []
    
    for v in videos:
         presentation["words"].extend( words[v]) 
         presentation["truths"].append( truth[v])
    presentation["words"] = list(set(presentation["words"]))
    presentation["truths"] = list(set(presentation["truths"]))
    presentation['specs']  = len(presentation["truths"])     
    if (not simple):
        presentation["video List"] = {} 
        for v in videos:
            presentation["video List"][v]= (words[v],truth[v])   
   
    return presentation

     

def complete_clustering(nb_step = 10,min_vid_clustering = 6):
    vec,kv,t,w,ws = separator( load_file())
    print(datetime.now(), "prepocessing end" )
    """
    root,intra,inter = one_stepSimple(vec)    
    f = open(str(paths.__stock / "result1KmeanMult.json"),'w')
    json.dump(root,f, sort_keys=True, indent=4, separators=(',', ': '))
    f.close()
    
    print(datetime.now(), "first step end" )
    """
    root = {'nbStep':nb_step,'vid_vec':vec}
    listToDo = deque([root])
    while len(listToDo) > 0:
        e = listToDo.popleft()
        node = e['nbStep'] > 1 and len(e['vid_vec']) >= min_vid_clustering
        e.update(convert_to_read_(list(e['vid_vec'].keys()),t,ws,node))
        e['number'] =  len(e['vid_vec'])
        if node:
            e['childs'] = []
            childs,total,inter,intra = one_stepSimple(e['vid_vec'])
            e['inertia'] = {'total':total,'inter':inter,'intra':intra}
            
            for c in childs.values():
                elem = {'nbStep':e['nbStep'] - 1,'vid_vec':c}
                e['childs'].append(elem)
                listToDo.append(elem)
        del e['vid_vec']
        del e['nbStep'] 
        
    file = "resultSpecMultiple-"+ str(len(vec)) +'.json'          
    f = open(str(paths.__stock / file),'w')   
    json.dump(root,f, sort_keys=True, indent=4, separators=(',', ': '))    
    f.close()
    """
    res = [recurs_clustering(root[k],truth,words,nb_step-1,(intra/inter)) for k in root.keys()]
    f = open(str(paths.__stock / "resultKmeanMult.json"),'w')
    json.dump(res,f, sort_keys=True, indent=4, separators=(',', ': '))
    """
    
if __name__ == '__main__':
    complete_clustering()  