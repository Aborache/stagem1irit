# -*- coding: utf-8 -*-
"""
Created on Wed May 22 10:28:15 2019

@author: Lenovo T420s
"""
from scipy.cluster.hierarchy import dendrogram, linkage
from matplotlib import pyplot as plt
import numpy as np
import TreeTools as TT
from sklearn.cluster import SpectralClustering
from datetime import datetime
import numpy as np

def timerSpectral():
    vec,kv,t,w,ws,n_v,n_a,n_f = TT.separator( TT.load_file())
    corpus = list(vec.values())
    
    tests = [500 * i for i in range(4,8) ]
    timesClass = []
    timesMatriceCreuse = []
    print(tests)
    for i in tests:
        print(i)
        context = np.array(corpus[:i])
        """
        t1 = datetime.now().timestamp()
        l = SpectralClustering(n_clusters=4, random_state=0,).fit(context).labels_
        t2 = datetime.now().timestamp()
        timesClass.append(t2-t1)
        print(t2-t1)
        """
        t1 = datetime.now().timestamp()
        matri = TT.SimilarityFilter(TT.SimilarityMatrix (TT.DistanceMatrix(context)))
        l = SpectralClustering(n_clusters=4,eigen_solver ='arpack',affinity='precomputed', random_state=0).fit(matri).labels_
        t2 = datetime.now().timestamp()
        timesMatriceCreuse.append(t2-t1)
        print(t2-t1)
        
    plt.figure(1)
    #plt.plot(tests, timesClass, 'r--')
    plt.plot(tests, timesMatriceCreuse, 'g--')
    plt.show()

def distancesDiff(nbVideo = 40):
    feat_name =["MeanNbFaces","StdNbFaces","ShotTransitionRate","InterVariation","IntraVariation","Interaction2Locteurs","Interaction3Locteurs","Interaction4Locteurs","Interaction4+Locteurs","InterventionShort","InterventionLong","SPonctuel","SLocalise","SPresent","SRegulier","SImportant","SpeakersDistribution","SpeakerTransition","Speech","Music","SpeechWithMusic","SpeechWithNoneMusic","NoneSpeechWithMusic","NoneSpeechWithNoneMusic","transcript"]
    for i in range (0, len(feat_name)):
        print(i,feat_name[i])
    vec,kv,t,w,ws,n_v,n_a,n_f = TT.separator( TT.load_file())
    features = {}
    cpt = 0
    for v,f in n_f.items():
        if f and cpt <nbVideo:
            features[v]= []
            for vid_feat in n_v[v]:
               features[v].append([vid_feat]) 
            for a in n_a[v]:
                features[v].append([a])
            features[v].append(vec[v])
            cpt += 1
            
    print("fin pretraitement")
    nb_features =  len(list(features.values())[0])  
    print(nb_features)         
    
    feature_matrixs =  {}
    
    for i in range (0 , nb_features):
        feature_matrixs[i] = []
        
    for  vid in features.values():
        cpt = 0
        for elem in vid:
             feature_matrixs[cpt].append(elem)
             cpt += 1
             
    new_feature_matrixs =   {}       
    for i in range (0 , nb_features):
        new_feature_matrixs[i] = np.array(feature_matrixs[i])
    
    distance_features = []
    for i in range (0 , nb_features):
        distance_features.append( TT.DistanceNormalization(TT.DistanceMatrix(new_feature_matrixs[i])))
    print("dif distance")
    final_features_dist = TT.DistanceMatrix(np.array(distance_features))
    plt.figure(1,(10,10))
    plt.matshow(final_features_dist)
    plt.colorbar()
    plt.show()
    
    """
    print(np.array(list(feature_matrixs.values())).shape)    
    cov_features = np.cov(np.array(list(feature_matrixs.values())))
    print("features cov")
    plt.matshow(cov_features)
    plt.colorbar()
    plt.show()
    """
    
        
    
    
    
 
if __name__ == '__main__':
    distancesDiff(3500)
    """
    timerSpectral()      
    """
""" 
X = [[i] for i in [2, 8, 0, 4, 1, 9, 9, 0]]



Z = linkage(X, 'ward')

print(Z)
Z = np.array([[1,0,0,2],[2,3,1,2],[4,5,2,2],[6,9,2,3],[7,8,3,4],[10,11,5,7]],dtype  = 'double' )
fig = plt.figure(figsize=(25, 10))
dn = dendrogram(Z)

plt.savefig("ici.jpg")

vec,kv,t,w,ws = TT.separator( TT.load_file())


Z = linkage(X, 'single')
dn = dendrogram(Z)
#plt.show()
plt.savefig("ici.jpg")

"""
