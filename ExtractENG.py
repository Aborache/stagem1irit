# -*- coding: utf-8 -*-
"""
Created on Mon May 20 10:06:13 2019

@author:  Mathieu Poquillon
"""

from Resources import paths
import xml.etree.ElementTree as ET
import os
import sys

stock = paths.__GENERAL__PATH__ / "WordEmbeding"
ENGListFileName = paths.ENGListFileName
listofVideo = paths.__FINAL_DESCRIPTORS_LIST_XML__

"""produce the list of nb video in english from the base corpus""" 
def extractor(listOfVideoNames,nb = 4000):
    if os.path.exists(listOfVideoNames):
        try:
            f = open(listOfVideoNames, 'r')
            file_names = f.read().splitlines()
            f.close()
        except IOError as e:
            print("Could not read file:", listOfVideoNames)
            print(str(e))
            sys.exit()
    else:
        print("File  " + str(listOfVideoNames) + " doesn't exist")
        sys.exit()
        

    if os.path.exists(str(ENGListFileName)):
        os.remove(str(ENGListFileName))
    f = open(str(ENGListFileName), 'a')
        
    numberENG = 0

    print("processing") 
    for line in file_names[:nb]:         
        inputFileXML = str(paths.__PATH_TO_DEV_DESCRIPTORS_L0__ / line )
        if os.path.exists(inputFileXML):
            try:
                tree = ET.parse(inputFileXML)
                root = tree.getroot()
                ENG = False
                
                for speechSegment in root.iter('SpeechSegment'):
                    if 'eng-usa' ==  speechSegment.get('lang'):
                       ENG = True
                if ENG:
                    f.write(line +"\n")
                    numberENG += 1

            except IOError as e:
                print("Could not append file:",inputFileXML )
                print(str(e))
            
        else:
            print("File " + inputFileXML + " doesn't exist !")
    f.close()
    print(str(numberENG) + " english videos  find" )
    
if __name__ == '__main__':
    extractor(listofVideo) 
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
       
        
    print("Extraction terminée")