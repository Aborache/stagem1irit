# -*- coding: utf-8 -*-
"""
Created on Mon May 20 11:57:53 2019

@author: Lenovo T420s
"""

import os
import sys
import xml.etree.ElementTree as ET
from nltk.corpus import stopwords
from Resources import paths
from sklearn.feature_extraction.text import TfidfVectorizer
import csv

__ground_truth = paths.__GENERAL__PATH__ / "Resources"/ "groundtruth.txt" 
__ground_truth_code =  paths.__GENERAL__PATH__ / "Resources"/ "codeGroundtruth.txt" 
def extractorL0File(inputFileXML,wFilter,proba = True,lang = 'eng-usa'):
    content = []
    if os.path.exists(inputFileXML):
        try:
            tree = ET.parse(inputFileXML)
            root = tree.getroot()
            for speechSegment in root.iter('SpeechSegment'):
                anyWord = False
                if lang ==  speechSegment.get('lang'):
                    sentence = []
                    prob = 0
                    time = 0
                    w =''
                    for word in speechSegment.iter('Word'):
                        t = word.get('stime')
                        p = word.get('conf')
                        if time != t or (not prob) :
                            if time != 0 and w not in wFilter:
                                sentence.append(w.lower())
                                anyWord = True
                            time = t
                            prob = p
                            w = word.text[:-1][1:]
                        elif prob < p:
                            w = word.text[:-1][1:]
                            prob = p

                    if time != 0 and w not in wFilter:
                                sentence.append(w)
                                anyWord = True    
                    if anyWord:
                        content.append(sentence)      
        except IOError as e:
            print("Could not load tree:", inputFileXML)
            print(str(e))
            sys.exit()
    return content

def extractorL0FileTag(inputFileXML):
    tags = []
    if os.path.exists(inputFileXML):
        try:
            tree = ET.parse(inputFileXML)
            root = tree.getroot()
            
            for tag in root.iter('Tag'):
                tags.append(tag.text)
        except IOError as e:
            print("Could not load tree:", inputFileXML)
            print(str(e))
            sys.exit()
            
    return tags
    
    
def extractorL0FileTimerSeparator(inputFileXML,wFilter,lang = 'eng-usa',timerSep= 7.0):
    content = []
    if os.path.exists(inputFileXML):
        try:
            tree = ET.parse(inputFileXML)
            root = tree.getroot()
            anyWord = False
            sentence = []
            prob = 0
            time = 0.0
            for speechSegment in root.iter('SpeechSegment'):
                
                w =''
                if lang ==  speechSegment.get('lang'):
                    
                    w =''
                    for word in speechSegment.iter('Word'):
                        t = word.get('stime')
                        p = word.get('conf')
                        if time != t or (not prob) :
                            if float(t) - float(time) > timerSep and anyWord:
                                content.append(sentence)
                                sentence = []
                                anyWord = False
                            
                            if time != 0 and w not in wFilter:
                                sentence.append(w.lower())
                                anyWord = True
                            time = t
                            prob = p
                            w = word.text[:-1][1:]
                        elif prob < p:
                            w = word.text[:-1][1:]
                            prob = p

            if time != 0.0 and w not in wFilter:
                sentence.append(w)
                anyWord = True 
            content.append(sentence)
            
        except IOError as e:
            print("Could not load tree:", inputFileXML)
            print(str(e))
            sys.exit()
    
    return content

def extractGroundTruth():
    with open(__ground_truth_code, newline='') as csvfile:
        code = {}
        spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
        for row in spamreader:
            code[int(row[0])] = row[1]
    truth = {}  
    with open(__ground_truth, newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
        for row in spamreader:
            truth[row[2]] = code[int(row[0])]
    
    return(truth)
    
def extractFeatures():
    code = {}
    with open(str(paths.__GENERAL__PATH__ / "FeaturesMatrix_audiovisual.txt")) as csvfile:
        
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        first = True
        for row in spamreader:
            if not first:
                code[str(row[0][:-4])] = [float(i) for i in row[1:]]
            else:
                first = False
                print(row)
    
    return code   
       

def testVidetimeSep(NamelistFile):

    if os.path.exists(NamelistFile):
        try:
            f = open(NamelistFile, 'r')
            file_names = f.read().splitlines()
            f.close()
        except IOError as e:
            print("Could not read file:", NamelistFile)
            print(str(e))
            sys.exit()
    else:
        print("File  " + str(NamelistFile) + " doesn't exist")
        sys.exit() 
    dictionary = {}
    for line in file_names:
        inputFileXML = str(paths.__PATH_TO_DEV_DESCRIPTORS_L0__ / line)
        filt = stopwords.words('english') + ['{fw}','.',',']
        
        words = extractorL0FileTimerSeparator(inputFileXML,filt)
        dictionary[line] = words
    return dictionary

def videoWordsmaker(NamelistFile):
    if os.path.exists(NamelistFile):
        try:
            f = open(NamelistFile, 'r')
            file_names = f.read().splitlines()
            f.close()
        except IOError as e:
            print("Could not read file:", NamelistFile)
            print(str(e))
            sys.exit()
    else:
        print("File  " + str(NamelistFile) + " doesn't exist")
        sys.exit() 
    dictionary = {}
    for line in file_names:
        inputFileXML = str(paths.__PATH_TO_DEV_DESCRIPTORS_L0__ / line)
        filt = stopwords.words('english') + ['{fw}','.',',']
        
        words = extractorL0File(inputFileXML,filt)
        newWords = []
        for sent in words:
            newWords.extend(sent)
        dictionary[line] = newWords
    return dictionary   

def newVideoWordsmaker(NamelistFile):
    if os.path.exists(NamelistFile):
        try:
            f = open(NamelistFile, 'r')
            file_names = f.read().splitlines()
            f.close()
        except IOError as e:
            print("Could not read file:", NamelistFile)
            print(str(e))
            sys.exit()
    else:
        print("File  " + str(NamelistFile) + " doesn't exist")
        sys.exit() 
    dictionary = {}
    tagDict =  {}
    for line in file_names:
        inputFileXML = str(paths.__PATH_TO_DEV_DESCRIPTORS_L0__ / line)
        filt = stopwords.words('english') + ['{fw}','.',','] 
        words = extractorL0File(inputFileXML,filt)
        tagDict[line] = extractorL0FileTag(inputFileXML)
        newWords = []
        for sent in words:
            newWords.extend(sent)
        dictionary[line] = newWords
    return dictionary, tagDict

def CorpusBuilder(nameAndWords):
    corpus = []
    for k, v in nameAndWords.items():
        s = ''
        for w in v:
            s += w + ' '
        corpus.append(s)
    return corpus

def weightsBuilder(nameAndWords,wordFilter = 0.10 ) :
    vectorizer = TfidfVectorizer()
    X = vectorizer.fit_transform(CorpusBuilder(nameAndWords))
    nameAndWordWeight =  {}
    cpt = 0
    wordList = vectorizer.get_feature_names()
    for k, v in nameAndWords.items():
        wg = {}
        #totalW = 0
        for w in v:
            try:
                c =  X[cpt,wordList.index(w)]
                if c > wordFilter:
                    wg[w] = c
                else :
                    wg[w] = 0
                #wg[w] = X[cpt,wordList.index(w)] ** 3
                #totalW += X[cpt,wordList.index(w)]
            except ValueError :
                wg[w] = 0 
            
        nameAndWordWeight[k] = wg        
        cpt += 1     
    return(nameAndWordWeight)
    
if __name__ == '__main__':
   r = extractFeatures()
