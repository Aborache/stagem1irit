# -*- coding: utf-8 -*-
"""
Created on Thu Jun 27 14:03:06 2019

@author: Lenovo T420s
"""
import numpy as np
from Resources import paths 
import json 
import matplotlib.pyplot as plt
from numpy import linalg as LA

"""normalize  a set of vectors"""
def normalization(matrix):
    return(matrix - np.min(matrix, axis = 0)) /(np.max(matrix,axis = 0) - np.min(matrix, axis = 0))

"""normalize a distance matrix"""
def DistanceNormalization(d):
    if (np.max(d) - np.min(d)) != 0:
        return(d - np.min(d)) /(np.max(d) - np.min(d))  
    else:
        return d

"""compute the distance matrix between the vectors of the set imput"""
def DistanceMatrix(v):
    return np.array([np.linalg.norm(v[i] -v[j]) for i in range(0,len(v)) for j in range (0,len(v))]).reshape((len(v),len(v)))

"""compute change a distance matrix to a similarity matrix"""    
def SimilarityMatrix(m,delta = 1):
    return np.exp(- m ** 2 / (2. * delta ** 2))

""" sub function for SimilarityFilter"""
def filt(x,f = 1 * 10 ** -5):
    if x > f:
        return x
    else:
        return 0.0
"""replace lower values of a similarity matrix with 0 for faster computation""" 
def SimilarityFilter(m):
    mf = np.vectorize(filt)
    return mf(m)
 
""" simple function to plot the efficiency of a the filter action on a similariy matrix""" 
def SimilarityFilterChange(Smatrix):
    new_Sim = SimilarityFilter(Smatrix)
    res = np.zeros(Smatrix.shape)
    for i in range (0,new_Sim.shape[0] ):
        for j in range (0,new_Sim.shape[1]):
            if i< j:
                res[i][j]= new_Sim[i][j]
            else:
                res[i][j]= Smatrix[i][j]
                
    plt.matshow(res)
    plt.colorbar()
    plt.show()    



"""load the precomputed corpus"""
def load_file():

    save_united = str(paths.__temp_file_video_to_united)
    fu = open(str(save_united),'r')
    
    u = json.load(fu)
    return u


"""from the corpus generate the dictionaries,
context = the transcript vector with mean ponderation methode for each video 
multi_context = the transcript vectors with ny personal cluster methode for each video   
name_truth = the truth for each video for each video  
name_word = the closest word to the contex vector for each video  
name_words = the closest words for each vector of multi context  for each video  
name_features = if there  features (audio, video) have been found for each video  
name_video = the vector represnting the video features for each video  
name_audio = the vector represnting the audio features for each video  
"""
def separator(united):
    context = {}
    multi_context = {}
    name_truth = {}
    name_word = {}
    name_words = {}
    name_video = {}
    name_audio = {}
    name_features = {}
    
    for  n, obj in united.items():
        context[n] = obj['meanVector']
        multi_context[n] = obj['KmeanVectors']
        name_truth[n] = obj['truth']
        name_word[n] = obj['meanWord']
        name_words[n] = obj['words']
        name_features[n] = obj["features"]
        if name_features[n]:
            name_video[n] = obj["f_video"]
            name_audio[n] = obj["f_audio"]
        
    return (context,multi_context,name_truth,name_word,name_words, name_video, name_audio, name_features)

""" compute the inertia values from one clustering """
def inertia(vectors,labels,nb_Clusters):
    gravity = np.mean(vectors,axis = 0)
    centers = {}
    number_cluster =  {}
    for e in range (0, nb_Clusters):
        centers[e] = []
        number_cluster[e] = 0
    
    for e in range (0, len(vectors)):
        centers[labels[e]].append(vectors[e])
        number_cluster[labels[e]] += 1
    
    for e in range (0, nb_Clusters):        
        centers[e] = np.mean(centers[e],axis = 0)
        
    total = np.mean([np.linalg.norm(gravity - vectors[i]) ** 2 for i in  range(0, vectors.shape[0])])
    inter = np.sum([(np.linalg.norm(gravity - centers[i]) ** 2) * number_cluster[i] for i in range(0, nb_Clusters)])/ vectors.shape[0] 
    intra = np.mean([np.linalg.norm(centers[i] - vectors[j]) ** 2
        for i in range (0 ,nb_Clusters)
        for j in range (0, vectors.shape[0]) if i == labels[j]])
    return total, inter,intra

"""compute the ineria mean values evolution on the tree"""
def inertia_evolution(root):
    intra_dic = {}
    inter_dic = {}
    total_dic = {}
    
    intra_leaf = []
    inter_leaf = []
    total_leaf = []
    
    nodes = [root]
    cpt = 0
    while len(nodes) > 0:
        new_nodes= []
        intra_dic[cpt] = list(intra_leaf)
        inter_dic[cpt] = list(inter_leaf)
        total_dic[cpt] = list(total_leaf)
        for n in nodes:
            
            
            
            intra_dic[cpt].append(n['inertia']['intra'])
            inter_dic[cpt].append(n['inertia']['inter'])
            total_dic[cpt].append(n['inertia']['total'])
            leaf = False
            
            for c in n['children']:
                if len(c['children']) == 0:
                    leaf = True
                else:
                    new_nodes.append(c)
            if leaf:
                intra_leaf.append(n['inertia']['intra'])
                inter_leaf.append(n['inertia']['inter'])
                total_leaf.append(n['inertia']['total'])
        cpt += 1
        nodes = []
        nodes.extend(new_nodes)
    intra = []
    inter = []
    total = [] 
    for i in range(0,cpt ):
        intra.append( np.mean(intra_dic[i]))
        inter.append(np.mean(inter_dic[i]))
        total.append( np.mean(total_dic[i])) 
        
    return(intra,inter,total,cpt)

        
    
""" from a set of video, create the node that will represent there group in the tree
videos = the name of each video of the group
truth = the dictionary which contain the truth for each video
words = the dictionary which contain the tags (multi context words) for each video""" 
def convert_to_read_(videos,truth,words,simple = True):
    presentation = {}
    presentation["videos"] = videos
    presentation["words"] = []
    presentation["truths"] = []
    
    truths = []
    for v in videos:
         presentation["words"].extend( words[v]) 
         truths.append( truth[v])
    presentation["words"] = list(set(presentation["words"]))
    presentation["truths"] = list(set(truths))
    presentation['specs']  = len(presentation["truths"])   
    presentation['purity'] = {'purity':compute_purity(truths,len(truth)) ,'majority' : sort_of_majority_purity(truths)}
    if (not simple):
        presentation["video List"] = {} 
        for v in videos:
            presentation["video List"][v]= (words[v],truth[v])   
        
    return presentation


"""compute the purity of a cluster from the list of cluster's video truth and the total number of video"""

def compute_purity(truths,total):
    var = list(set(truths))
    dic = {}
    for v in var :
        dic[v] = 0
    
    for t in truths :
        dic[t] += 1
    distribution = np.array(list(dic.values()))
    
    return(np.max(distribution) / total )
    
"""compute a intra cluster purity from  from the list of cluster's video truth   """
def sort_of_majority_purity(truths):
    var = list(set(truths))
    dic = {}
    for v in var :
        dic[v] = 0
    
    for t in truths :
        dic[t] += 1
    distribution = np.array(list(dic.values()))
    maximum = np.max(distribution)
    
    maj_truth = ''
    for t,val in dic.items():
        if val == maximum:
            maj_truth = t
            
    return(maximum / np.sum(distribution) ,maj_truth)


"""compute the purity evolution on the tree"""
def all_purity(root) :
    nodes = [root]
    tot_pur = []
    purity_truth = {}
    purity_truth_mean = {}
    number_clusters_purity =  {}
    nb_nodes = 0
    for t in root['truths']:
        purity_truth[t] = []

        
    while len(nodes) > 0:
        n = nodes.pop()
        if len(n['children']) > 0:
            nodes.extend(n['children'])
        else:
            score, t = n['purity']['majority']
            purity_truth[t].append(score)
            tot_pur.append(score)
            nb_nodes += 1
    for k, v in purity_truth.items():
        if len(v)> 0:
            purity_truth_mean[k] = np.mean(v)
            number_clusters_purity[k] = len(v)
        else:
            purity_truth_mean[k] = float('nan')
            number_clusters_purity[k] = 0
    tot_moy = np.mean(tot_pur)
    return(tot_moy,nb_nodes,purity_truth_mean,number_clusters_purity)    
            
            
       
"""produce a dendogram compatible linkage from a tree"""
def tree_to_linkage(tree):
    if tree['stop']:
        return []
    
    new_linkage = []  
    nodes = [tree]
    leafs = []
    labels = []
    join_cpt = 0
    cpt = 0
    deep = 0
    while (cpt < len(nodes)):
        node = nodes[cpt]
        for child in node['children']:
            if child['stop']:
                leafs.append(child)
            else:
                nodes.append(child)
        cpt += 1
    
    while(join_cpt < len(leafs)):
        leafs[join_cpt]['id_linkage'] = join_cpt
        labels.append(leafs[join_cpt]['id'])
        leafs[join_cpt]['qt'] = 1
        deep = max(deep,leafs[join_cpt]['nbStep'])
        join_cpt += 1
        
    while len(nodes ) > 0:
        n = nodes.pop()        
        first = True 
        cpt = 0
        for c in n['children']:
            if first :
                first = False
                primal = c
            else:
                p = {}
                p['qt'] = primal['qt'] + c['qt']
                p['nbStep'] = primal['nbStep']
                p['id_linkage'] = join_cpt
                new_linkage.append([primal['id_linkage'],c['id_linkage'],deep - p['nbStep'],p['qt']])
                join_cpt += 1
                del c['id_linkage']
                del c['qt']
                del primal['id_linkage']
                del primal['qt']
                primal = p
                
        n['id_linkage'] = primal['id_linkage']
        n['qt'] = primal['qt']
        
    del tree['id_linkage']
    del tree['qt']
    
    return(new_linkage,labels)        

"""del the stop parameter from each node of the tree for a safe save on json format """    
def tree_sup_stop(root):
    if root['stop']:
        del root['stop']
        return root
    
    nodes = [root]
    cpt = 0
    while (cpt < len(nodes)):
        node = nodes[cpt]
        for child in node['children']:
            if not(child['stop']):
                nodes.append(child)
            del child['stop']
        cpt += 1
    return(root)
    
    
    
    
"""implementation of Sandrine Mouysset's code  Error in the imlementation DON'T USE this"""
def Criterion(Partition,DistMatrix):
    print(len(Partition))

    # code from Sandrine Mouysset
    DistMatrix=DistMatrix+np.transpose(DistMatrix)
    # Define the vector which reorder data point per cluster assignment
    PartitionSC=[0];
    nbpercluster=np.zeros((max(Partition)+1,1))
    nbpercluster2=np.zeros((max(Partition)+1,1))
    cpt=0
    for i in range(1,max(Partition)+1):
        for j in range(0,len(Partition)):
            if Partition[j]==i:
                PartitionSC.append(int(j))
                nbpercluster[i]=nbpercluster[i]+1
        nbpercluster2[i]=nbpercluster2[i-1]+nbpercluster[i]
    
    nbpercluster=np.int_(nbpercluster2)    
    
    # Reorder the distance/similarity Matrix per cluster               
    DistMatrixOrd=DistMatrix[PartitionSC,:]
    DistMatrixOrd=DistMatrixOrd[:,PartitionSC]
    # plot of the reordered distance/similarity matrix
    plt.matshow(DistMatrixOrd)
    plt.colorbar()
    plt.show()               
               
    
    
    
    # Clustering Criterion : evaluate the mean ratio between the off diagonal blocks which represent 
    # the distance/similarity between the clusters and the diagonal ones which represent the 
    # distance/similarity within the clusters
    BlocClusters=np.zeros((max(Partition),max(Partition)))
    print(max(Partition))
    Criterion=0
    for i in range(1,max(Partition)+1):
        #print(i)
        for j in range(i,max(Partition)+1):
            #print(j)
            t=np.arange(1,nbpercluster[i]-(nbpercluster[i-1]+1),dtype=np.int)
            u=np.arange(1,nbpercluster[j]-(nbpercluster[j-1]+1),dtype=np.int)
            
            #Compute the frobenius norm of each distance/similarity cluster submatrix
            BlocClusters[i-1,j-1]=LA.norm([DistMatrixOrd[nbpercluster[i-1]+k,nbpercluster[j-1]+p]
                                           for k in t
                                           for p in u],'fro')
            # Compute the criterion
            if i!=j:
                Criterion=Criterion+1/(len(Partition)^2)*(BlocClusters[i-1,j-1]/(max(BlocClusters[i-1,i-1],1)))
    
    print('Mesure des ration de Frobenius', Criterion)   
                    
    #Plot of the measures between the clusters
    plt.matshow(BlocClusters)
    plt.colorbar()
    plt.show()
    