# -*- coding: utf-8 -*-
"""
Created on Mon May 20 16:06:45 2019

@author: Poquillon
"""

from sklearn.cluster import KMeans
from sklearn.cluster import AgglomerativeClustering

import numpy as np

import gensim
from gensim.scripts.glove2word2vec import glove2word2vec
from gensim.test.utils import datapath, get_tmpfile
from datetime import datetime
from collections import deque

import generateWordEmbeding as WE
import TextExtractor as tx
from Resources import paths
from WordEmbeding import ContextProcessing as cProces
from WordEmbeding import VideoToVect as vtv
from WordEmbeding import meanContextProcessing as mC


saveFileTxT = WE.stock /'cluster.txt'
vectorisorAdr = WE.vectorisorAdr
VideoENG = WE.stock / "ENGVideoList.txt"
vectorisorAdrGloveTwitter = paths.__GENERAL__PATH__ / 'glove' / 'glove.twitter.27B.200d.txt'
vectorisorAdrGloveWiki = paths.__GENERAL__PATH__ / 'glove' / 'glove.6B.200d.txt'
tmpFile = WE.stock / 'glovConver.txt.'
  

def treeBuildHirarclust(root,tree,clustersNbV, clustersvector,listCluster,nbVidTot,wE,contextDesc ):
    if root in listCluster:
        return clustersNbV[root],clustersvector[root]
    else:
        node = tree[root - nbVidTot]
        lnb,lv = treeBuildHirarclust(node[0],tree,clustersNbV, clustersvector,listCluster,nbVidTot,wE,contextDesc)
        rnb,rv = treeBuildHirarclust(node[1],tree,clustersNbV, clustersvector,listCluster,nbVidTot,wE,contextDesc)
        vect = ((lnb * lv) + (rnb * rv)) / (lnb + rnb)
        
        listDesc = wE.similar_by_vector(vect,contextDesc)
        desc = ''
        for d in listDesc:
            desc += str(d[0]) + ' '
        Descriptor = '\nid:' + str(root) + ' context:' + str(desc) +' nbVideos:'+ str(lnb + rnb) + '\n fils:' + str([node[0],node[1]])
        f = open(str(saveFileTxT),'a')
        f.write(Descriptor)
        f.close()
        return (lnb +rnb), vect

def hierarchycalClusteringMatrix(nameAndWords, wE, weigth, contextDesc = 3, nbCluster= 50):
    f = open(str(saveFileTxT),'a')
    stepDescriptor = 'hierarchycalClusteringMatrix, length:' + str(len(nameAndWords))
    f.write(stepDescriptor)
    f.close()
    
    print(datetime.now(), "prepocessing hierarchycalClustering" )
    VideoVectors,videoSucc = cProces.videosContext(nameAndWords, wE,weigth, contextDesc)
    matrix=[]
    for v1 in nameAndWords.keys():
        matrix.append(VideoVectors[v1])
    
    Distmat = cProces.distanceMatrixComputation(matrix)
    a = AgglomerativeClustering(n_clusters = nbCluster, affinity="precomputed",linkage = "average" )
    print(datetime.now(), "processing" )
    a.fit(Distmat)
    print(datetime.now(), "writing the results" )
    clusterslabel = a.labels_
    #print(Distmat)
    #print(a.labels_)
    clusters = {}
    for i in range(0,nbCluster):
        clusters[i] = []
        
    cpt = 0    
    for v1 in nameAndWords.keys():
        clusters[clusterslabel[cpt]].append(v1)
        cpt += 1
        
    for i in range(0,nbCluster):
        stepDescriptor = '\n \n new cluster :' + str(i)  + " size :" +  str(len(clusters[i])) +'\n'
        f = open(str(saveFileTxT),'a')
        f.write(stepDescriptor)
        f.close()
        for video in clusters[i]:
            stepDescriptor = video +' words:'
            for vect in VideoVectors[video]:
                stepDescriptor += ' ' +str(wE.similar_by_vector(vect,1)[0] )
            stepDescriptor += '\n'
            f = open(str(saveFileTxT),'a')
            f.write(stepDescriptor)
            f.close()
    return(1)

def hierarchycalClusteringVideoToVect(nameAndWords ,VectorSize = 300, nbCluster= 50):
    f = open(str(saveFileTxT),'a')
    stepDescriptor = 'hierarchycalClusteringVideoToVect, length:' + str(len(nameAndWords))
    f.write(stepDescriptor)
    f.close()
    
    print(datetime.now(), "prepocessing hierarchycalClustering" )
    VideoVectors,docVect = vtv.videosContext(nameAndWords,VectorSize,recompute = True ,saveModel = True)

    Distmat = vtv.distanceMatrixComputation(VideoVectors,docVect)
    a = AgglomerativeClustering(n_clusters = nbCluster, affinity="precomputed",linkage = "complete" )
    print(datetime.now(), "processing" )
    a.fit(Distmat)
    print(datetime.now(), "writing the results" )
    clusterslabel = a.labels_
    clusters = {}
    for i in range(0,nbCluster):
        clusters[i] = []
        
    cpt = 0    
    for v1 in nameAndWords.keys():
        clusters[clusterslabel[cpt]].append(v1)
        cpt += 1
        
    for i in range(0,nbCluster):
        stepDescriptor = '\n \n new cluster :' + str(i)  + " size :" +  str(len(clusters[i])) +'\n'
        f = open(str(saveFileTxT),'a')
        f.write(stepDescriptor)
        f.close()
        for video in clusters[i]:
            stepDescriptor = video + '\n'
            f = open(str(saveFileTxT),'a')
            f.write(stepDescriptor)
            f.close()
    return(1)  
    
    
def hierarchycalClustering(nameAndWords, wE, weight, contextDesc = 3, nbCluster= 50):
    dicoVect = {}
    print(datetime.now(), "prepocessing hierarchycalClustering" )
    for v1 in nameAndWords.keys():
        #dicoVect[v1] = WE.meanVectorCalculator2(nameAndWords[v1],wE)
        dicoVect[v1] = WE.meanVectorCalculatorWeight(nameAndWords[v1],wE,weight[v1])
    
    clusters = {}
    clusters['global']= nameAndWords.keys()
    stepDescriptor = 'hierarchycalClustering, length:' + str(len(nameAndWords))
    f = open(str(saveFileTxT),'a')
    f.write(stepDescriptor)
    f.close()
    print(datetime.now(), "processing" )
    a = AgglomerativeClustering(n_clusters = 50, linkage="ward")
    
    nb = len(dicoVect)
    matrix =  np.zeros((nb,WE.vector_dim))            
    cpt = 0
    
    for v in dicoVect:
        matrix[cpt] = dicoVect[v]
        cpt += 1
    a.fit(matrix)
    print(datetime.now(), "postprocessing hierarchycalClustering" )
    nbElem = matrix.shape[0]

    nodeToCompute = deque([2 * nbElem - 2])
    Clusters = []
    while ( (len(Clusters) + len(nodeToCompute)) < nbCluster ):
        node = a.children_ [nodeToCompute.popleft() - nbElem  ]
        for f in (node[0],node[1]):
            if f < nbElem:
                Clusters.append(f)
            else:
                nodeToCompute.append(f)
    Clusters.extend(nodeToCompute)
    
    clustersVideos = {}
    clustersvector = {}
    clustersContext = {}
    clustersNBVid = {}
    key = list(nameAndWords.keys())
    for c in Clusters:
        cvi  = []
        cvec = []
        cc = deque([c])
        while (len(cc)>0):
            subc = cc.popleft()
            if subc < nbElem:
                cvi.append(key[subc])
                cvec.append(subc)
            else:
                node = a.children_ [subc - nbElem  ]
                cc.extend([node[0],node[1]])
        clustersVideos[c] = cvi
        clustersvector[c] = np.mean(matrix[cvec],axis=0 )
        clustersNBVid[c]= len(clustersVideos[c])
        listDesc = wE.similar_by_vector(clustersvector[c],contextDesc)
        desc = ''
        for d in listDesc:
            desc += str(d[0]) + ' '
        clustersContext[c] = desc
        Descriptor = '\nid:' + str(c) + ' context:' + str(clustersContext[c]) +' nbVideos:'+ str(clustersNBVid[c]) + '\n videos:' + str(clustersVideos[c])
        f = open(str(saveFileTxT),'a')
        f.write(Descriptor)
        f.close()  
        
    """tree"""
    r = 2 * nbElem - 2
    treeBuildHirarclust(r,a.children_ ,clustersNBVid, clustersvector,Clusters,nbElem,wE,contextDesc )    
    
    return 1

def partialClustering(nameAndWords,wE,weight,repeat = 5, children = 2,contextDesc = 3):
    stepDescriptor = 'partialClustering, length:' + str(len(nameAndWords)) + ' children:' + str(children)
    f = open(str(saveFileTxT),'a')
    f.write(stepDescriptor)
    f.close()
    dicoVect = {}
    print(datetime.now(), "prepocessing partialClustering" )
    for v1 in nameAndWords.keys():
        dicoVect[v1] = dicoVect[v1] = WE.meanVectorCalculatorWeight(nameAndWords[v1],wE,weight[v1])
    
    clusters = {}
    clusters['global']= nameAndWords.keys()

    for step in range(0,repeat) :   
        print(datetime.now(), "step", step )
        newsClusters = {}
        stepDescriptor = "\nNew step " + str(step) +'\n'
        for key in clusters.keys():
            cluster = clusters[key]
            stepDescriptor += "\ncontext = " + key + ' nb:' +str(len(cluster)) +  ':\n'
            if(len(cluster)) > children*10:
                nb = len(cluster)
                c =  np.zeros((nb,WE.vector_dim))
                
                cpt = 0
                for video in cluster:
                    c[cpt] = dicoVect[video]
                    cpt += 1
                matrix = c
                k = KMeans(n_clusters=children, random_state=0).fit(matrix)
                lab = k.labels_
                
                res = []
                meanResVect = []
                for n1 in range(0,children):
                    res.append([])
                    meanResVect.append(0)
                
                cv = 0
                for v in cluster:                
                    res[lab[cv]].append(v)
                    meanResVect[lab[cv]] += c[cv]
                    cv += 1
                
                for n2 in range(0,children):
                    if len(res[n2]) > 0:
                        listDesc = wE.similar_by_vector((meanResVect[n2] / len(res[n2])),contextDesc)
                        desc = ''
                        for d in listDesc:
                            desc += str(d[0]) + ' '
                        newsClusters[desc] = res[n2]
                        stepDescriptor += ' ' + desc + ' NBVIDEO:' + str(len(res[n2]))
                        stepDescriptor += '\n'
            else:
                stepDescriptor += ' videos ' + str(cluster) + '\n'
        """sauvegarde fichier"""        
        f = open(str(saveFileTxT),'a')
        f.write(stepDescriptor)
        f.close()
        clusters = newsClusters
        
    stepDescriptor = "final résult \n"
    for key in clusters.keys():
        
        cluster = clusters[key]
        stepDescriptor += "context = " + key + ' nb:' +str(len(cluster)) +':\n'
        stepDescriptor += ' videos ' + str(cluster) + '\n'
        
    f = open(str(saveFileTxT),'a')
    f.write(stepDescriptor)
    f.close()
    clusters = newsClusters    
    return(clusters)
    
def completeClustering(nameAndWords,listOfWE, children = 3,contextDesc = 3):
    stepDescriptor = 'completeClustering,length:' + str(len(nameAndWords)) + ' children:' + str(children)
    f = open(str(saveFileTxT),'a')
    f.write(stepDescriptor)
    f.close()
    
#    boucle (WE)
    clusters = {}
    clusters['global']= nameAndWords.keys()
    cptg = 0
    for wEnb in listOfWE:        
        newsClusters = {}
        cptg += 1
        stepDescriptor = "\nNew step " + str(cptg) +'\n'
        for key in clusters.keys():
            cluster = clusters[key]
            stepDescriptor += "\ncontext = " + key + ' nb:' +str(len(cluster)) +  ':\n'
            if(len(cluster)) > children *5:
                nb = len(cluster)
                c =  np.zeros((nb,WE.vector_dim))
                
                cpt = 0
                for video in cluster:
                    c[cpt] = WE.MeanVectorCalculator(nameAndWords[video],wEnb)
                    cpt += 1
                matrix = c
                k = KMeans(n_clusters=children, random_state=0).fit(matrix)
                lab = k.labels_
                
                res = []
                meanResVect = []
                for n1 in range(0,children):
                    res.append([])
                    meanResVect.append(0)
                
                cv = 0
                for v in cluster:                
                    res[lab[cv]].append(v)
                    meanResVect[lab[cv]] += c[cv]
                    cv += 1
                
                for n2 in range(0,children):
                    listDesc = wEnb.similar_by_vector((meanResVect[n2] / len(res[n2])),contextDesc)
                    desc = ''
                    for d in listDesc:
                        desc += str(d[0]) + ' '
                    newsClusters[desc] = res[n2]
                    stepDescriptor += ' ' + desc + ' NBVIDEO:' + str(len(res[n2]))
                    stepDescriptor += '\n'
            else:
                stepDescriptor += ' videos ' + str(cluster) + '\n'
        """sauvegarde fichier"""        
        f = open(str(saveFileTxT),'a')
        f.write(stepDescriptor)
        f.close()
        clusters = newsClusters
        
    stepDescriptor = "final résult " + str(cpt) +'\n'
    for key in clusters.keys():
        stepDescriptor += "context = " + key + ':\n'
        cluster = clusters[key]
        stepDescriptor += ' videos ' + str(cluster) + '\n'
    f = open(str(saveFileTxT),'a')
    f.write(stepDescriptor)
    f.close()
    clusters = newsClusters    
    return(clusters)

def simpleComparaison(nameAndWords,wordsEmb,weigth,fileToSave):
    VideoVectorsV1,videoPerti1 = cProces.videosContext(nameAndWords, wordsEmb,weigth, 4)
    videoVectorsV2,videoPerti2 = mC.videosContext(nameAndWords, wordsEmb,weigth)
    for i in VideoVectorsV1.keys():
        if (videoPerti1[i] and videoPerti2[i]):
            stepDescriptor = '\nname:' + str(i)      
            stepDescriptor += ' \nclustering context :'
            for j in VideoVectorsV1[i]:
               stepDescriptor += ' ' + str(wordsEmb.similar_by_vector(j,1)[0] )
            stepDescriptor +='\n meancontext' + str(wordsEmb.similar_by_vector(videoVectorsV2[i],1)[0] )   
            f = open(str(fileToSave),'a')
            f.write(stepDescriptor)
            f.close()   
    return 1

def totalComparaison(nameAndWords,wordsEmbList,weigth,fileToSave):
    videoMeanvector = {}
    videoPertMean = {}
    videoClusteringVectors = {} 
    videoPertClust = {}
    
    for k in wordsEmbList.keys():
        videoMeanvector[k] ,videoPertMean[k]  =  mC.videosContext(nameAndWords, wordsEmbList[k],weigth)
        videoClusteringVectors[k] , videoPertClust[k] = cProces.videosContext(nameAndWords, wordsEmbList[k],weigth, 4)  
        
    for v in nameAndWords.keys():
        writeResult = True 
        for k in wordsEmbList.keys():
            writeResult = videoPertMean[k][v] and videoPertClust[k][v] and writeResult
        if writeResult:
            stepDescriptor = '\n\nname:' + str(v)   + " size:" + str(len(nameAndWords[v]))             
            for k in wordsEmbList.keys():
                stepDescriptor +='\n' + str(k) +' \nclustering context :'
                for j in videoClusteringVectors[k][v]:
                   stepDescriptor += ' ' + str(wordsEmbList[k].similar_by_vector(j,1)[0] )
                stepDescriptor +='\n meancontext' + str(wordsEmbList[k].similar_by_vector(videoMeanvector[k][v],1)[0] )   
            f = open(str(fileToSave),'a')
            f.write(stepDescriptor)
            f.close()
        
    return 1

def separatorResult(listOfVideoNames,wordsEmb,weight,fileToSave = (WE.stock /'meanSep.txt') ) :
    nameAndWordLists = tx.testVidetimeSep(listOfVideoNames)
    
    
    context ,result = mC.videosMultipleContexts(nameAndWordLists,wordsEmb, weight)  
    print("bonjour")    
    for k in context.keys():
        
        stepDescriptor = "\n " + str(k)
        for elem in context[k]:
            stepDescriptor += " " + str(wordsEmb.similar_by_vector(elem,1)[0] )
        f = open(str(fileToSave),'a')
        f.write(stepDescriptor)
        f.close()
    print("aurevoir") 
            
def demonstrateur(listOfVideoNames):
    print(datetime.now(), "start " )
    
    
    wordsEmb = gensim.models.KeyedVectors.load_word2vec_format(tmpFile)
    tfid = tx.weightsBuilder(tx.videoWordsmaker(listOfVideoNames))
    separatorResult(listOfVideoNames,wordsEmb,tfid)
    
    """
    dictionary = tx.videoWordsmaker(listOfVideoNames)
    print(datetime.now(), "words by video done " )
    
    wEbibli = {}    
    wEbibli["autotrain"] = gensim.models.KeyedVectors.load(vectorisorAdr, mmap='r')
    
    glove_file = datapath(str(vectorisorAdrGloveWiki))    
    tmp_file = get_tmpfile(str(tmpFile))
    
    _ = glove2word2vec(glove_file, tmp_file)
    wEbibli["Wiki"] = gensim.models.KeyedVectors.load_word2vec_format(tmpFile)
    
    glove_file = datapath(str(vectorisorAdrGloveTwitter))    
    tmp_file = get_tmpfile(str(tmpFile))
    _ = glove2word2vec(glove_file, tmp_file)
    wEbibli["twitter"] = gensim.models.KeyedVectors.load_word2vec_format(tmpFile)
    
    print(datetime.now(), "vectorisor load " )
    
    tfid = tx.weightsBuilder(dictionary)
    print(datetime.now(), "weights by video word done " )
    totalComparaison(dictionary,wEbibli,tfid,(WE.stock /'comparaisonTot0.15.txt'))
 
    #hierarchycalClusteringVideoToVect(dictionary ,VectorSize = 300, nbCluster= 100)

    WElist = []
    for i in range (0,2):
        WElist.append(gensim.models.KeyedVectors.load(vectorisorAdr, mmap='r'))
    completeClustering(dictionary,WElist)
    
    
    glove_file = datapath(str(vectorisorAdrGlove))
    
    tmp_file = get_tmpfile(str(tmpFile))
    _ = glove2word2vec(glove_file, tmp_file)
    """
    #vectorisor = gensim.models.KeyedVectors.load_word2vec_format(tmpFile)
    #vectorisor = gensim.models.KeyedVectors.load(vectorisorAdr, mmap='r')
    #print(datetime.now(), "vectorisor load " )
    #partialClustering(dictionary,vectorisor,repeat = 6, children = 2,contextDesc = 3)    
    #hierarchycalClustering(dictionary,vectorisor)
    #simpleComparaison(dictionary, vectorisor,tfid,(WE.stock /'comparaison.txt'))
    #hierarchycalClusteringMatrix(dictionary, vectorisor,tfid, contextDesc = 2)
    

if __name__ == '__main__':
    demonstrateur(VideoENG)
        
        
        
    
               
            
            
                
            
                
                
            
#       apprend model pour taille fenetre
#       pour chaque video 
#           calcul du vecteur moyen
#       pour chaque groupe
#            calcul des sous groupes
#            pour chaque sous groupe
#               calcul du mot contexte
#    
#           
    
    
        
    
    
    
    
    
    
    