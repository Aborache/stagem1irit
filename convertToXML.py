'''
Created on Jun 28, 2017

@author: Hasan AL Jawad
'''
from lxml import etree
#import xml.etree.ElementTree as etree
import os
import sys
import traceback
from numpy import mean
import codecs
from Resources import paths

__path_DEV_DESCRIPTORS_L1__ = paths.__PATH_TO_DEV_DESCRIPTORS_L1__
__path_listOfVideoNames__ = paths.__FINAL_DESCRIPTORS_LIST_XML__

__DESCRIPTORS_ORDER__ = "Interaction2Locteurs;Interaction3Locteurs;Interaction4Locteurs;Interaction4+Locteurs;InterventionShort;InterventionLong;SPonctuel;SLocalise;SPresent;SRegulier;SImportant;SpeakersDistribution;SpeakerTransition;Speech;Music;SpeechWithMusic;SpeechWithNoneMusic;NoneSpeechWithMusic;NoneSpeechWithNoneMusic;MeanNbFaces;StdNbFaces;ShotTransitionRate;InterVariation;IntraVariation"
__DELIMITER__ = ","

def meanOfList(ListAsString):
    """
    Takes a string that represents a list and returns the mean value of this list.
    @param ListAsString: String in the format of "Item_1,Item_2,...,Item_n"
    """
    stringList = ListAsString.split(",")
    floatList = []
    for item in stringList:
        floatList.append(float(item))
    
    return mean(floatList)


def toXML(listOfVideoNames, pathToL1, outputFile):
    
    if os.path.exists(listOfVideoNames):
        try:
            f = open(listOfVideoNames, 'r')
            file_names = f.read().splitlines()
            f.close()
        except IOError as e:
            print("Could not read file:", listOfVideoNames)
            print(str(e))
            sys.exit()
    else:
        print("File " + str(listOfVideoNames) + " doesn't exist")
        sys.exit()
        
    videosList = etree.Element("VideosList")
    descriptorsOrder = etree.SubElement(videosList, "DescriptorsOrder")
    descriptorsOrder.text = __DESCRIPTORS_ORDER__
    
    for line in file_names:
        try:
            print(line)
            tree = etree.parse(str(pathToL1 / line))
            root = tree.getroot()
            
            descriptorsList = root.findall('Descriptors')
            
            video_element = etree.SubElement(videosList, "Video")
            video_element.set("name", line)
            segments = dict()
            transcripts = dict()
             
            #consider only the last descriptors list 
            #descriptorsList = [descriptorsList[len(descriptorsList) - 1]] 
            
            #consider only the first descriptors list 
            #descriptorsList = [descriptorsList[0]] 
                
            for descriptors in descriptorsList:
                descriptors_element = etree.SubElement(video_element, "Descriptors")
                descriptors_element.set("nbSegments", descriptors.get('nbSegments'))
                
                for index in range(int(descriptors.get('nbSegments'))):
                    segments[index+1] = etree.SubElement(descriptors_element, "Segment")
                    segments[index+1].set("numSegment", str((index+1)))
                    segments[index+1].text = ""
                    
                    transcripts[index+1] = etree.SubElement(descriptors_element, "Transcript")
                    transcripts[index+1].set("numSegment", str((index+1)))
                    #transcripts[index+1].set("lang", descriptors.find("VideoTranscript").get("lang"))
                    transcripts[index+1].text = ""
                
                #-------------------------Transcripts--------------------------  
                videoTranscripts = descriptors.find("VideoTranscript")
                for words in videoTranscripts:
                    transcripts[int(words.get("numSegment"))].text = words.text
                
                #-------------------------Interactions--------------------------    
                interactions = descriptors.find("Interactions")
                for inter in interactions:
                    segments[int(inter.get("numSegment"))].text += inter.text + __DELIMITER__
                    
                #-------------------------Interventions--------------------------       
                interventions = descriptors.find("Interventions")
                for inter in interventions:
                    segments[int(inter.get("numSegment"))].text += inter.text + __DELIMITER__
                
                #-------------------------SpeakersTypeList--------------------------     
                speakersTypeList = descriptors.find("SpeakersTypeList")
                for speakerType in speakersTypeList:
                    for speaker in speakerType:
                        segments[int(speakerType.get("numSegment"))].text += speaker.text + __DELIMITER__
                
                #-------------------------SpeakersDistributionList--------------------------     
                speakersDistribution = descriptors.find("SpeakersDistribution")
                for speaker in speakersDistribution:
                    segments[int(speaker.get("numSegment"))].text += speaker.text + __DELIMITER__
                    
                #-------------------------NumberSpeakerTransitionList--------------------------     
                numberSpeakerTransitionList = descriptors.find("NumberSpeakerTransitionList")
                for speakerTransition in numberSpeakerTransitionList:
                    segments[int(speakerTransition.get("numSegment"))].text += speakerTransition.text + __DELIMITER__
                    
                #-------------------------SpeechMusicAlignmentList--------------------------     
                speechMusicAlignmentList = descriptors.find("SpeechMusicAlignmentList")
                for sml in speechMusicAlignmentList:
                    for type in sml:
                        segments[int(sml.get("numSegment"))].text += type.text + __DELIMITER__
                    
                #-------------------------nbFacesList--------------------------     
                nbFacesList = descriptors.find("nbFacesList")
                for nbFaces in nbFacesList:
                    segments[int(nbFaces.get("numSegment"))].text += nbFaces.find("MeanNbFaces").text + __DELIMITER__
                    segments[int(nbFaces.get("numSegment"))].text += nbFaces.find("StdNbFaces").text + __DELIMITER__
                    
                #-------------------------NumberShotTransitionList--------------------------     
                numberShotTransitionList = descriptors.find("NumberShotTransitionList")
                for shot in numberShotTransitionList:
                    segments[int(shot.get("numSegment"))].text += shot.text + __DELIMITER__
                    
                #-------------------------InterIntensityVariationList--------------------------
                IntensityVariation = descriptors.find("IntensityVariation/InterIntensityVariation/InterIntensityVariationList")
                if int(descriptors.get("nbSegments")) <= 1:
                    segments[1].text += "0" + __DELIMITER__ + str(meanOfList(IntensityVariation[0].text[1:-1])) + __DELIMITER__
                else:
                    for shot in IntensityVariation:
                        segments[int(shot.get("numSegment"))].text += str(meanOfList(shot.text[1:-1])) + __DELIMITER__
                    segments[int(descriptors.get("nbSegments"))].text += "0" + __DELIMITER__
        except:
            traceback.print_exc()
    
    with codecs.open(outputFile, 'wb', 'utf8') as f:
        #videosList.write(f)
        result =(etree.ElementTree(videosList))
        f.write( etree.tostring(videosList, encoding ='unicode'))

    #with open(outputFile,"w") as f:
     #   f.write(etree.tostring(videosList, pretty_print=True))
    

if __name__ == '__main__':
    toXML(__path_listOfVideoNames__, __path_DEV_DESCRIPTORS_L1__, os.getcwd() + "/DescriptorsList.xml")
    #toXML(__path_listOfVideoNames__, __path_DEV_DESCRIPTORS_L1__, os.getcwd() + "/DescriptorsList_firstlevel.xml")