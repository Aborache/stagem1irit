# -*- coding: utf-8 -*-
"""
Created on Thu Jul  4 09:52:19 2019

@author: Lenovo T420s
"""
import matplotlib.pyplot as plt

import TreeTools as TT
import numpy as np
from sklearn.cluster import SpectralClustering

"""compare the distance using different features"""
def Calculator(number = 40):
    vec,kv,t,w,ws,n_v,n_a,n_f = TT.separator( TT.load_file())
    
    matrix_mean = []
    matrix_kmeans = []
    vid_matrix = []
    aud_matrix = []
    invertDict = {}
    Dict = {}
    cpt = 0
    
    for v1,context in vec.items():
        if n_f[v1] and cpt < number:
            print(cpt,v1,t[v1])
            matrix_kmeans.append(kv[v1][0])
            matrix_mean.append(context)  
            
            vid_matrix.append(n_v[v1])
            aud_matrix.append(n_a[v1])
            
            invertDict[cpt]= v1
            Dict[v1] = cpt
            cpt+= 1
    
    vid_matrix = TT.normalization(vid_matrix)
    aud_matrix = TT.normalization(aud_matrix)
    
    vid_Dist = TT.DistanceMatrix(vid_matrix)
    vid_Dist_Norm = TT.DistanceNormalization(vid_Dist)
    
    aud_Dist = TT.DistanceMatrix(aud_matrix)
    aud_Dist_Norm = TT.DistanceNormalization(aud_Dist)
    
    

    trans_mean_Dist = TT.DistanceMatrix(np.array(matrix_mean))
    trans_mean_Dist_Norm = TT.DistanceNormalization(trans_mean_Dist)
    
    trans_kmeans_Dist = TT.DistanceMatrix(np.array(matrix_kmeans))
    trans_kmeans_Dist_Norm = TT.DistanceNormalization(trans_kmeans_Dist)
    
    fig = plt.figure(1,(10,10))
    ax = fig.add_subplot(221)
    ax.imshow(vid_Dist_Norm)
    ax = fig.add_subplot(222)
    ax.imshow(aud_Dist_Norm)
    ax = fig.add_subplot(223)
    ax.imshow(trans_kmeans_Dist_Norm)
    ax = fig.add_subplot(224)
    ax.imshow(trans_mean_Dist_Norm)

    
    plt.show()
    
def TestCodeSandrine(number = 40):
   """ 
   r = TT.SimilarityFilter(np.array([[0.0000001,0.0001],[0.0000000001,0.1]]))
   print(r)
   """ 
    
   vec,kv,t,w,ws,n_v,n_a,n_f = TT.separator( TT.load_file())
    
   matrix_mean = []
   matrix_kmeans = []
   vid_matrix = []
   aud_matrix = []
   invertDict = {}
   Dict = {}
   cpt = 0
    
   for v1,context in vec.items():
        if n_f[v1] and cpt < number:
            print(cpt,v1,t[v1])
            matrix_kmeans.append(kv[v1][0])
            matrix_mean.append(context)  
            
            vid_matrix.append(n_v[v1])
            aud_matrix.append(n_a[v1])
            
            invertDict[cpt]= v1
            Dict[v1] = cpt
            cpt+= 1
   """"
   vid_matrix = TT.normalization(vid_matrix)
   aud_matrix = TT.normalization(aud_matrix)
    
   vid_Dist = TT.DistanceMatrix(vid_matrix)
   vid_Dist_Norm = TT.DistanceNormalization(vid_Dist)
    
   aud_Dist = TT.DistanceMatrix(aud_matrix)
   aud_Dist_Norm = TT.DistanceNormalization(aud_Dist)
    
    
   """
   trans_mean_Dist = TT.DistanceMatrix(np.array(matrix_mean))
   trans_mean_Dist_Norm = TT.DistanceNormalization(trans_mean_Dist)
   TT.SimilarityFilterChange(TT.SimilarityMatrix(trans_mean_Dist_Norm))


   trans_kmeans_Dist = TT.DistanceMatrix(np.array(matrix_kmeans))
   trans_kmeans_Dist_Norm = TT.DistanceNormalization(trans_kmeans_Dist)
   TT.SimilarityFilterChange(TT.SimilarityMatrix(trans_kmeans_Dist_Norm))

   
   
   for i in (2,3,4,6):
       print("number of cluster",i)
       matri = TT.SimilarityFilter(TT.SimilarityMatrix (trans_mean_Dist_Norm))
       l = SpectralClustering(n_clusters=i,eigen_solver ='arpack',affinity='precomputed', random_state=0,).fit(matri).labels_
       TT.Criterion(l,matri)
       print(len(l))
       matri = TT.SimilarityFilter(TT.SimilarityMatrix (trans_kmeans_Dist_Norm))
       l = SpectralClustering(n_clusters=i,eigen_solver ='arpack',affinity='precomputed', random_state=0,).fit(matri).labels_
       TT.Criterion(l,matri)
   

    
if __name__ == '__main__':
    #Calculator()
    TestCodeSandrine()
