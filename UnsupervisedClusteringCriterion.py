
# coding: utf-8

# In[2]:


import seaborn as sns; sns.set()
import numpy as np
from matplotlib.pyplot import imshow
import matplotlib.pyplot as plt


f = open('FeatureMatrix.txt', 'r')
DataMatrix = [[float(num) for num in line.split(',')] for line in f]
DataFrame=np.asarray(DataMatrix)
corr = np.corrcoef(DataFrame)
print(corr)

# Plot of correlation matrices from raw data
imshow(np.asarray(corr))




# In[14]:


import numpy as np
from numpy import linalg as LA
from sklearn import metrics
from matplotlib.pyplot import imshow
import matplotlib.pyplot as plt

# import Partition from clustering methods and convert in vector of integers
g = open('PartitionSC.txt', 'r')
Partition=[int(line)  for line in g]
Partition=np.int_(Partition)
# import distance or similarity matrices and convert in array
f = open('dist_matrix.txt', 'r')

DistMatrix = [[float(num) for num in line.split(',')] for line in f]
DistMatrix=np.asarray(DistMatrix)
DistMatrix=DistMatrix+np.transpose(DistMatrix)

# Plot Distance or similarity Matrix 
imshow(np.asarray(DistMatrix))

# Define the vector which reorder data point per cluster assignment
PartitionSC=[0];
nbpercluster=np.zeros((max(Partition)+1,1))
nbpercluster2=np.zeros((max(Partition)+1,1))
cpt=0
for i in range(1,max(Partition)+1):
    for j in range(0,len(Partition)):
        if Partition[j]==i:
            PartitionSC.append(int(j))
            nbpercluster[i]=nbpercluster[i]+1
    nbpercluster2[i]=nbpercluster2[i-1]+nbpercluster[i]

nbpercluster=np.int_(nbpercluster2)    

# Reorder the distance/similarity Matrix per cluster               
DistMatrixOrd=DistMatrix[PartitionSC,:]
DistMatrixOrd=DistMatrixOrd[:,PartitionSC]
# plot of the reordered distance/similarity matrix
plt.matshow(DistMatrixOrd)
plt.colorbar()
plt.show()               
           



# Clustering Criterion : evaluate the mean ratio between the off diagonal blocks which represent 
# the distance/similarity between the clusters and the diagonal ones which represent the 
# distance/similarity within the clusters
BlocClusters=np.zeros((max(Partition),max(Partition)))
print(max(Partition))
Criterion=0
for i in range(1,max(Partition)+1):
    #print(i)
    for j in range(i,max(Partition)+1):
        #print(j)
        t=np.arange(1,nbpercluster[i]-(nbpercluster[i-1]+1),dtype=np.int)
        u=np.arange(1,nbpercluster[j]-(nbpercluster[j-1]+1),dtype=np.int)
        
        #Compute the frobenius norm of each distance/similarity cluster submatrix
        BlocClusters[i-1,j-1]=LA.norm([DistMatrixOrd[nbpercluster[i-1]+k,nbpercluster[j-1]+p]
                                       for k in t
                                       for p in u],'fro')
        # Compute the criterion
        if i!=j:
            Criterion=Criterion+1/(len(Partition)^2)*(BlocClusters[i-1,j-1]/(max(BlocClusters[i-1,i-1],1)))

print('Mesure des ration de Frobenius', Criterion)   
                
#Plot of the measures between the clusters
plt.matshow(BlocClusters)
plt.colorbar()
plt.show()



# In[13]:


plt.matshow(BlocClusters)
plt.colorbar()
plt.show()

