# -*- coding: utf-8 -*-
"""
Created on Tue Jun 11 13:37:52 2019

@author: Lenovo T420s
"""
import TextExtractor as tx
import matplotlib.pyplot as plt
import generateWordEmbeding as WE
import os
import sys
import xml.etree.ElementTree as ET
from Resources import paths

VideoENG = WE.stock / "ENGVideoList.txt"


def extractorL0FileWordDuration(inputFileXML,lang = 'eng-usa'):
    content = []
    if os.path.exists(inputFileXML):
        try:
            tree = ET.parse(inputFileXML)
            root = tree.getroot()
            time = 0.0
            firstWord = True 
            for speechSegment in root.iter('SpeechSegment'):
                if lang ==  speechSegment.get('lang'):                    
                    for word in speechSegment.iter('Word'):
                        t = float(word.get('stime'))
                        if (t != time):
                            if not firstWord:
                                content.append(t - time)
                            else:
                                firstWord = False
                            time = t
                              
        except IOError as e:
            print("Could not load tree:", inputFileXML)
            print(str(e))
            sys.exit()
    return content

def timeSepDistribution():
    if os.path.exists(VideoENG):
        try:
            f = open(VideoENG, 'r')
            file_names = f.read().splitlines()
            f.close()
        except IOError as e:
            print("Could not read file:", VideoENG)
            print(str(e))
            sys.exit()
    else:
        print("File  " + str(VideoENG) + " doesn't exist")
        sys.exit() 
        
    separators = []
    for line in file_names:
        inputFileXML = str(paths.__PATH_TO_DEV_DESCRIPTORS_L0__ / line)
        separators.extend( extractorL0FileWordDuration(inputFileXML))
    
    separators = [s for s in separators if s < 20]
    separators = [s for s in separators if s > 1]
    plt.hist(separators,bins= 100)
    plt.show() 


def test():
    plt.hist([1,2,3,4,5,6,6])
    plt.show()

def poidtfidf(Filelist):
    dicti = tx.videoWordsmaker(Filelist)
    tfid = tx.weightsBuilder(dicti)
    
    cpt = 0
    tfidflist = []
    for k,i in dicti.items():
        weight = tfid[k]
        tfidflist.extend( [weight[w] for w in i])
        cpt +=1
    plt.hist(tfidflist,bins= 100)
    plt.show() 
    detailList = [i for i in tfidflist if i > 0.05]    
    plt.hist(detailList,bins= 100)
    plt.show()     
        

if __name__ == '__main__':
    timeSepDistribution()
    #poidtfidf(VideoENG)    