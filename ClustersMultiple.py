# -*- coding: utf-8 -*-
"""
Created on Mon Jun 17 11:11:25 2019

@author: Lenovo T420s
"""
from sklearn.cluster import KMeans

import json
import numpy as np
from Resources import paths
from datetime import datetime
import matplotlib.pyplot as plt
import math
from collections import deque
from sklearn.cluster import SpectralClustering
from scipy.cluster.hierarchy import dendrogram

import TreeTools as TT

"""from videos's vectors, compute a clustering using kmeans or spectral with the best posible number of chidren"""
def one_stepSimple(video_vect,method = 'kmeans',min_var = 0.1):
    
    nb = len(video_vect)
    context = np.array(list(video_vect.values()))
    if method == 'kmeans':
        l = KMeans(n_clusters=2, random_state=0).fit(context).labels_
    elif method == 'spectral':
        matri = TT.SimilarityFilter(TT.SimilarityMatrix(TT.DistanceNormalization(TT.DistanceMatrix(np.array(list(video_vect.values()))))))
        l = SpectralClustering(n_clusters=2,eigen_solver ='arpack',affinity='precomputed', random_state=0,).fit(matri).labels_
        
  
    tot,inter_c, inert = TT.inertia(context,l,2)
    nbc = 2
    nodes = [2]
    inertias = [inert]
    inter_class = [inter_c]
    total =[tot]
    score_evolution = [inert/inter_c]
    stop = False
    nb_node = 3 
    while((not stop) and nb_node < int(math.sqrt(nb))):
        if method == 'kmeans':
            lb = KMeans(n_clusters=nb_node, random_state=0).fit(context).labels_
        elif method == 'spectral':
            lb = SpectralClustering(n_clusters=nb_node,eigen_solver ='arpack',affinity='precomputed', random_state=0,).fit(matri).labels_
            
        tb, cb,ib = TT.inertia(context,lb,nb_node)
        nodes.append(nb_node)
        inertias.append(ib)
        inter_class.append(cb)
        total .append(tb)
        score_evolution.append(ib/cb)
        stop = abs((inert/inter_c) - (ib/cb)) < min_var
        if (ib/cb) < (inert/inter_c):
            l = lb
            inert = ib
            inter_c = cb
            nbc = nb_node
            tot = tb
        
        nb_node += 1    
    
    """
    plt.figure(1,figsize=(25, 10))
    plt.subplot(411)
    plt.plot(nodes, inertias, 'r--')
    plt.subplot(412)
    plt.plot( nodes,inter_class, 'bs')
    plt.subplot(413)
    plt.plot( nodes,total, 'v--')
    plt.subplot(414)    
    plt.plot(nodes, score_evolution, 'g^')
    plt.savefig('result.jpg')
    
    
    print(l,inert,inter_c,nbc)
    """
    cpt = 0
    clusters = {}
    for i in range (0,nbc):
        clusters[i] = {}
        
    for name,vec in video_vect.items():
        clusters[l[cpt]][name] = vec
        cpt += 1
    return(clusters,tot,inter_c,inert)
"""from video context vector, audio vector and video vector compute a clustering using spectral
 with the best posible number of chidren
 with he the possibility of assigning a weight to each one"""
def one_stepTVA(video_vect,t_weigth,v_weigth,a_weigth,min_var = 0.1):
    
    nb = len(video_vect)
    """producing the Similarity matrix"""
    transcript = np.array([val[0] for val in video_vect.values()])
    video = np.array([val[1] for val in video_vect.values()])
    audio = np.array([val[2] for val in video_vect.values()])
    
    dist_audio = TT.DistanceNormalization(TT.DistanceMatrix(audio))
    dist_video = TT.DistanceNormalization(TT.DistanceMatrix(video))
    dist_trans = TT.DistanceNormalization(TT.DistanceMatrix(transcript))
    
    final_dist = (dist_audio * a_weigth + dist_video * v_weigth + dist_trans * t_weigth) / (a_weigth + v_weigth + t_weigth)
    matri = TT.SimilarityFilter(TT.SimilarityMatrix (TT.DistanceMatrix(final_dist)))


    """computation"""
    l = SpectralClustering(n_clusters=2,eigen_solver ='arpack',affinity='precomputed', random_state=0,).fit(matri).labels_
  
    t_tot,t_inter_c, t_inert = TT.inertia(transcript,l,2)
    v_tot,v_inter_c, v_inert = TT.inertia(video,l,2)
    a_tot,a_inter_c, a_inert = TT.inertia(audio,l,2)
    
    tot = (a_tot * a_weigth + v_tot * v_weigth + t_tot * t_weigth) / (t_weigth+v_weigth+a_weigth)
    inter_c = (a_inter_c * a_weigth + v_inter_c * v_weigth + t_inter_c * t_weigth) / (t_weigth+v_weigth+a_weigth)
    inert = (a_inert * a_weigth + v_inert * v_weigth + t_inert * t_weigth) / (t_weigth+v_weigth+a_weigth)
    
    nbc = 2
    nodes = [2]
    inertias = [inert]
    inter_class = [inter_c]
    total =[tot]
    score_evolution = [inert/inter_c]
    stop = False
    nb_node = 3 
    while((not stop) and nb_node < int(math.sqrt(nb))):
        
        lb = SpectralClustering(n_clusters=nb_node,eigen_solver ='arpack',affinity='precomputed', random_state=0,).fit(matri).labels_
        
        t_tot,t_inter_c, t_inert = TT.inertia(transcript,lb,nb_node)
        v_tot,v_inter_c, v_inert = TT.inertia(video,lb,nb_node)
        a_tot,a_inter_c, a_inert = TT.inertia(audio,lb,nb_node)
        tb = (a_tot * a_weigth + v_tot * v_weigth + t_tot * t_weigth) / (t_weigth+v_weigth+a_weigth)
        cb = (a_inter_c * a_weigth + v_inter_c * v_weigth + t_inter_c * t_weigth) / (t_weigth+v_weigth+a_weigth)
        ib = (a_inert * a_weigth + v_inert * v_weigth + t_inert * t_weigth) / (t_weigth+v_weigth+a_weigth)
        
        nodes.append(nb_node)
        inertias.append(ib)
        inter_class.append(cb)
        total .append(tb)
        score_evolution.append(ib/cb)
        stop = abs((inert/inter_c) - (ib/cb)) < min_var
        if (ib/cb) < (inert/inter_c):
            l = lb
            inert = ib
            inter_c = cb
            nbc = nb_node
            tot = tb
        
        nb_node += 1

    cpt = 0
    clusters = {}
    for i in range (0,nbc):
        clusters[i] = {}
        
    for name,vec in video_vect.items():
        clusters[l[cpt]][name] = vec
        cpt += 1
    return(clusters,tot,inter_c,inert)
"""realise a complete clustering with multiple subdivision using text audio and video features"""
def new_complete_clustering(nb_step_max = 6,min_vid_clustering = 6,kmeansContext =False,t_weigth = 1,v_weigth = 1,a_weigth = 1): 
    vec,kv,t,w,ws,n_v,n_a,n_f = TT.separator( TT.load_file())
    """selection of video that have a feature vector and normalization"""
    
    matrix=[]
    vid_matrix = []
    aud_matrix = []
    invertDict = {}
    Dict = {}
    cpt = 0
    vectors = {}
    for v1,context in vec.items():
        if n_f[v1]:
            if kmeansContext:
                matrix.append(kv[v1][0])
                vectors[v1] = kv[v1][0]
            else:
                matrix.append(context)  
                vectors[v1] = context
            
            vid_matrix.append(n_v[v1])
            aud_matrix.append(n_a[v1])
            
            invertDict[cpt]= v1
            Dict[v1] = cpt
            cpt+= 1
    
    print(cpt) 
    """normalisation of the vector because each featureneed to have the same  influance  """     
    vid_matrix = TT.normalization(vid_matrix)
    aud_matrix = TT.normalization(aud_matrix)
    vect_3 = {}
    
    for k, vect in vectors.items():
        vect_3[k] = [vect,vid_matrix[Dict[k]],aud_matrix[Dict[k]]]
    
    root = {'id':'0','nbStep':0,'vid_vec':vect_3,'stop' : False}
    listToDo = deque([root])
    list_node = []
    list_stock = []
    leaf_cpt = 0
    leafs = []
    while len(listToDo) > 0:
        e = listToDo.popleft()
        node = e['nbStep'] < nb_step_max and len(e['vid_vec']) >= min_vid_clustering and not e['stop']
        e.update(TT.convert_to_read_(list(e['vid_vec'].keys()),t,ws,node))
        e['quantity'] =  len(e['vid_vec'])
        if node:
            e['children'] = []
            #childs,total,inter,intra = one_stepTVA(e['vid_vec'],t_weigth * (1 + e['nbStep']) , v_weigth / (1 + e['nbStep']),a_weigth / (1 + e['nbStep']))
            childs,total,inter,intra = one_stepTVA(e['vid_vec'],t_weigth , v_weigth,a_weigth )
            e['inertia'] = {'total':total,'inter':inter,'intra':intra}
            leaf = intra < inter
            for k in childs.keys():
                c = childs[k]
                elem = {'id':e['id'] + str(k),'nbStep':e['nbStep'] + 1,'vid_vec':c,'stop': leaf }
                e['children'].append(elem)
                listToDo.append(elem)
            list_node.append(e)
        
        else:
            e['qt'] = 1
            e['id-dendro'] = leaf_cpt
            leaf_cpt += 1
            leafs.append(e)
        
        list_stock.append(e) 
        del e['stop']
        del e['vid_vec']
    print(datetime.now(), "computation end" )
    description = "Complet-spectral"+"-"+ str(len(vec))+"-"+str(t_weigth)+"-"+ str(v_weigth)+"-" + str(a_weigth )
    if kmeansContext:
        description += 'kcontext'
    "stat"    
    inter_class_evolution = [[]for i in range(0,nb_step_max)]
    intra_class_evolution = [[]for i in range(0,nb_step_max)]
    tot_inertia_evolution = [[]for i in range(0,nb_step_max)]
        
    dendo = []
    id_next = leaf_cpt
    
    while len(list_node ) > 0:
        n = list_node.pop()
        inter = n['inertia']['inter']
        inter_class_evolution[n['nbStep']].append(n['inertia']['inter'])
        intra_class_evolution[n['nbStep']].append(n['inertia']['intra'])
        tot_inertia_evolution[n['nbStep']].append(n['inertia']['total'])
        
        first = True 
        for c in n['children']:
            if first :
                first = False
                primal = c
            else:
                p = {}
                p['qt'] = primal['qt'] + c['qt']
                p['nbStep'] = primal['nbStep']
                p['id-dendro'] = id_next
                dendo.append([primal['id-dendro'],c['id-dendro'],p['nbStep'],p['qt']])
                id_next += 1
                del c['id-dendro']
                del c['qt']
                del primal['id-dendro']
                del primal['qt']
                primal = p
                
                
        n['id-dendro'] = primal['id-dendro']
        n['qt'] = primal['qt']

    inter_class_evolution_mean = []
    intra_class_evolution_mean = []
    tot_inertia_evolution_mean = []
    for i in range(0,nb_step_max ):
        inter_class_evolution_mean.append( np.mean(inter_class_evolution[i]))
        intra_class_evolution_mean.append(np.mean(intra_class_evolution[i]))
        tot_inertia_evolution_mean.append( np.mean(tot_inertia_evolution[i]))

    nodes = [i for i in range(0,nb_step_max)]
    plt.figure(1,figsize=(25, 10))
    #plt.subplot(311)
    plt.plot(nodes, intra_class_evolution_mean, 'r--')
    #plt.subplot(312)
    plt.plot( nodes,inter_class_evolution_mean, 'g--')
    #plt.subplot(313)
    plt.plot( nodes,tot_inertia_evolution_mean, 'b--')
    plt.savefig(str(paths.__stock /('evolution-inertia-(intra,inter,tot)'+ description+'.jpg')))
       
     
    """     
    fig = plt.figure(2,(250, 100))
    dn = dendrogram(np.array(dendo,dtype  = 'double'))    
    plt.savefig(str(paths.__stock /('dendro-'+ description+'.jpg')) ) 
    """
    
    size_var = [0 for i in range(0,150)]
    size_nb_video = [0 for i in range(0,150)]
    truth_var = [0 for i in range(0,26)]    
    for lf in leafs:
        if lf['quantity'] > 2 and lf['quantity'] < 150:
            size_var[lf['quantity']-3] += 1
            size_nb_video[lf['quantity']-3] += lf['quantity']-3
            truth_var[lf['specs']-1] += 1
            
    size_cumul = [size_nb_video[0]]
    for i in range(1,150):
        size_cumul.append(size_cumul[-1] + size_nb_video[i])    
        
    
    plt.figure(3,figsize=(50, 30))
    plt.subplot(411)
    plt.plot([i for i in range(3,153)], size_var, 'r--')
    plt.subplot(412)
    plt.plot([i for i in range(3,153)], size_nb_video, 'g--')
    plt.subplot(413)
    plt.plot([i for i in range(3,153)], size_cumul, 'v--')
    plt.subplot(414)
    plt.plot( [i for i in range(1,27)],truth_var, 'b--')     
    plt.savefig(str(paths.__stock /('Cluster_3_stat(size,nbtruth)-'+ description+'.jpg')) )
    print(size_var,truth_var)
    "save"
    print(datetime.now(), "stat end" )

    file = "result-"+ description +'.json'
       
    f = open(str(paths.__stock / file),'w')   
    json.dump(root,f, sort_keys=True, indent=4, separators=(',', ': '))    
    f.close()
    
    leafFile = "leafs-"   + description +'.json'
    f = open(str(paths.__stock / leafFile),'w')   
    json.dump(leafs,f, sort_keys=True, indent=4, separators=(',', ': '))    
    f.close()
    print(datetime.now(), "end" )
    
       
    

     
"""complete clustering for the text vector or for a vector that is a combinaison of text, video and audio"""
def complete_clustering(nb_step_max = 10,min_vid_clustering = 6,method = 'kmeans',kmeansContext =False,one_united_vector = False):
    vec,kv,t,w,ws,w_v,w_a,f_f = TT.separator( TT.load_file())
    
    if one_united_vector:
        vect =  {}
        vid_matrix = TT.normalization(list(w_v.values()))
        aud_matrix = TT.normalization(list(w_a.values()))
        cpt = 0
        for k,disp in f_f.items():
            if disp:
                if kmeansContext:
                    vect[k] = kv[k][0]
                else:
                    vect[k] = vec[k]
                vect[k].extend(list(vid_matrix[cpt]) + list(aud_matrix[cpt]))
                
                cpt += 1
        
        
    else:    
        if kmeansContext:
            vect = {}
            for k,i in kv.items():
                vect[k] = i[0]
        else:
            vect = vec
                  
    print(datetime.now(), "prepocessing end" )

    root = {'id':'0','nbStep':0,'vid_vec':vect,'stop' : False}
    listToDo = deque([root])
    list_node = []
    list_stock = []
    leaf_cpt = 0
    leafs = []
    while len(listToDo) > 0:
        e = listToDo.popleft()
        node = e['nbStep'] < nb_step_max and len(e['vid_vec']) >= min_vid_clustering and not e['stop']
        e.update(TT.convert_to_read_(list(e['vid_vec'].keys()),t,ws,node))
        e['quantity'] =  len(e['vid_vec'])
        if node:
            e['children'] = []
            childs,total,inter,intra = one_stepSimple(e['vid_vec'],method)
            e['inertia'] = {'total':total,'inter':inter,'intra':intra}
            leaf = total < 6 or intra < inter
            for k in childs.keys():
                c = childs[k]
                elem = {'id':e['id'] + str(k),'nbStep':e['nbStep'] + 1,'vid_vec':c,'stop': leaf }
                e['children'].append(elem)
                listToDo.append(elem)
            list_node.append(e)
        
        else:
            e['qt'] = 1
            e['id-dendro'] = leaf_cpt
            leaf_cpt += 1
            leafs.append(e)
        
        list_stock.append(e) 
        del e['stop']
        del e['vid_vec']
    print(datetime.now(), "computation end" )
    description = str(method)+"-"+ str(len(vec))
    if kmeansContext:
        description += 'kcontext'
    if one_united_vector:
        description += 'one_vector'
    "stat"    
    inter_class_evolution = [[]for i in range(0,nb_step_max)]
    intra_class_evolution = [[]for i in range(0,nb_step_max)]
    tot_inertia_evolution = [[]for i in range(0,nb_step_max)]
        
    dendo = []
    id_next = leaf_cpt
    
    while len(list_node ) > 0:
        n = list_node.pop()
        inter = n['inertia']['inter']
        inter_class_evolution[n['nbStep']].append(n['inertia']['inter'])
        intra_class_evolution[n['nbStep']].append(n['inertia']['intra'])
        tot_inertia_evolution[n['nbStep']].append(n['inertia']['total'])
        
        first = True 
        for c in n['children']:
            if first :
                first = False
                primal = c
            else:
                p = {}
                p['qt'] = primal['qt'] + c['qt']
                p['nbStep'] = primal['nbStep']
                p['id-dendro'] = id_next
                dendo.append([primal['id-dendro'],c['id-dendro'],p['nbStep'],p['qt']])
                id_next += 1
                del c['id-dendro']
                del c['qt']
                del primal['id-dendro']
                del primal['qt']
                primal = p
                
                
        n['id-dendro'] = primal['id-dendro']
        n['qt'] = primal['qt']

    inter_class_evolution_mean = []
    intra_class_evolution_mean = []
    tot_inertia_evolution_mean = []
    for i in range(0,nb_step_max ):
        inter_class_evolution_mean.append( np.mean(inter_class_evolution[i]))
        intra_class_evolution_mean.append(np.mean(intra_class_evolution[i]))
        tot_inertia_evolution_mean.append( np.mean(tot_inertia_evolution[i]))

    nodes = [i for i in range(0,nb_step_max)]
    plt.figure(1,figsize=(10,5))
    #plt.subplot(311)
    plt.plot(nodes, intra_class_evolution_mean, 'r--')
    #plt.subplot(312)
    plt.plot( nodes,inter_class_evolution_mean, 'g--')
    #plt.subplot(313)
    plt.plot( nodes,tot_inertia_evolution_mean, 'b--')
    plt.savefig(str(paths.__stock /('evolution-inertia-(intra,inter,tot)'+ description+'.jpg')))
       
     
         
    fig = plt.figure(2,(5, 10))
    """
    dn = dendrogram(np.array(dendo,dtype  = 'double'))    
    plt.savefig(str(paths.__stock /('dendro-'+ description+'.jpg')) ) 
    """
    
    size_var = [0 for i in range(0,150)]
    size_nb_video = [0 for i in range(0,150)]
    truth_var = [0 for i in range(0,26)]    
    for lf in leafs:
        if lf['quantity'] > 2 and lf['quantity'] < 150:
            size_var[lf['quantity']-3] += 1
            size_nb_video[lf['quantity']-3] += lf['quantity']-3
            truth_var[lf['specs']-1] += 1
            
    size_cumul = [size_nb_video[0]]
    for i in range(1,150):
        size_cumul.append(size_cumul[-1] + size_nb_video[i])    
        
    
    plt.figure(3,figsize=(10, 10))
    plt.subplot(411)
    plt.plot([i for i in range(3,153)], size_var, 'r--')
    plt.subplot(412)
    plt.plot([i for i in range(3,153)], size_nb_video, 'g--')
    plt.subplot(413)
    plt.plot([i for i in range(3,153)], size_cumul, 'v--')
    plt.subplot(414)
    plt.plot( [i for i in range(1,27)],truth_var, 'b--')     
    plt.savefig(str(paths.__stock /('Cluster_3_stat(size,nbtruth)-'+ description+'.jpg')) )
    print(size_var,truth_var)
    "save"
    print(datetime.now(), "stat end" )

    file = "result-"+ description +'.json'
       
    f = open(str(paths.__stock / file),'w')   
    json.dump(root,f, sort_keys=True, indent=4, separators=(',', ': '))    
    f.close()
    
    leafFile = "leafs-"   + description +'.json'
    f = open(str(paths.__stock / leafFile),'w')   
    json.dump(leafs,f, sort_keys=True, indent=4, separators=(',', ': '))    
    f.close()
    print(datetime.now(), "end" )
    


"""compute the thema using the text vector (context) and produce a tree that while expend after in a gender clustering using audio and video"""    
def thema_clustering(vect,truth,ws,kmeansContext = False, nb_step_max = 10,min_vid_clustering = 40 ,min_cluster = 6,method = 'spectral',min_var = 0.1):    
    """
    vec,kv,t,w,ws,w_v,w_a,f_f = TT.separator( TT.load_file())
    vect =  {}
    for k,i in f_f.items():
        if i:
            if kmeansContext:
                vect[k] = kv[k][0]
            else:
                vect[k] = vec[k]
    else:
        vect = vec
    """              
    print(datetime.now(), "prepocessing end" )

    root = {'id':'0','nbStep':0,'vid_vec':vect,'stop' : False}
    listToDo = deque([root])
    list_node = []
    list_stock = []
    leafs = []
    nb_step_max_real = 0
    while len(listToDo) > 0:
        e = listToDo.popleft()
        nb_step_max_real = max(nb_step_max_real,e['nbStep'])
        node = e['nbStep'] < nb_step_max and len(e['vid_vec']) >= min_vid_clustering and not e['stop']
        e.update(TT.convert_to_read_(list(e['vid_vec'].keys()),truth,ws,node))
        e['quantity'] =  len(e['vid_vec'])
        e['children'] = []
        if node:            
            childs,total,inter,intra = one_stepSimple(e['vid_vec'],method,min_var)
            e['inertia'] = {'total':total,'inter':inter,'intra':intra}
            leaf = total < 6 or intra < inter
            cluster_centers = []
            to_reatribute = {}
            for k in childs.keys():
                c = childs[k]
                if len(c) > min_cluster:
                    cent = np.mean(list(c.values()),axis = 0)
                    cluster_centers.append( {'center' :cent,'cluster': c })
                    elem = {'id':e['id'] + str(k),'nbStep':e['nbStep'] + 1,'vid_vec':c,'stop': leaf }
                    e['children'].append(elem)
                    listToDo.append(elem)
                    list_node.append(e)
                else:
                    for name,vector in c.items():
                        to_reatribute[name] = vector
                        
            if len(cluster_centers) > 0:      
                for name,vector in  to_reatribute.items():
                    pos = 0
                    score = np.linalg.norm(np.array(vector) - cluster_centers[0]['center'])
                    for cpt in range (1,len(cluster_centers)):
                        new_score = np.linalg.norm(np.array(vector) - cluster_centers[cpt]['center'])
                        if new_score < score:
                            score = new_score
                            pos = cpt
                    cluster_centers[pos]['cluster'][name] = vector
            
                    
                
            else:     
                elem = {'id':e['id'] + str(k),'nbStep':e['nbStep'] + 1,'vid_vec':to_reatribute,'stop': True }
                e['children'].append(elem)
                listToDo.append(elem)
                list_node.append(e)
        
        else:
            e['stop'] = True
            leafs.append(e)
        
        list_stock.append(e) 
        e['stop']
        del e['vid_vec']
        
    print(datetime.now(), "computation end" )
    """
    description = 'thema' +str(min_var)+ str(method)+"-"+ str(len(vec)) 
    if kmeansContext:
        description += 'kcontext'
   
    
    inter_class_evolution = [[]for i in range(0,nb_step_max_real)]
    intra_class_evolution = [[]for i in range(0,nb_step_max_real)]
    tot_inertia_evolution = [[]for i in range(0,nb_step_max_real)]
   
    dendo = []
    


    while len(list_node ) > 0:
        n = list_node.pop()
        inter = n['inertia']['inter']
        inter_class_evolution[n['nbStep']].append(n['inertia']['inter'])
        intra_class_evolution[n['nbStep']].append(n['inertia']['intra'])
        tot_inertia_evolution[n['nbStep']].append(n['inertia']['total'])
       
    dendo,l = TT.tree_to_linkage(root)
    plt.figure(2,(40, 5)) 
    dendrogram(np.array(dendo,dtype  = 'double'),labels = l)    
    plt.savefig(str(paths.__stock /('dendro-'+ description+'.jpg')) )
    inter_class_evolution_mean = []
    intra_class_evolution_mean = []
    tot_inertia_evolution_mean = []
    for i in range(0,nb_step_max_real ):
        inter_class_evolution_mean.append( np.mean(inter_class_evolution[i]))
        intra_class_evolution_mean.append(np.mean(intra_class_evolution[i]))
        tot_inertia_evolution_mean.append( np.mean(tot_inertia_evolution[i]))
    
       
    print('mean cluster sort of purity', np.mean([l['purity']['majority'][0] for l in leafs ])  )
    
    nodes = [i for i in range(0,nb_step_max_real)]
    plt.figure(1,figsize=(10,5))
    #plt.subplot(311)
    plt.plot(nodes, intra_class_evolution_mean, 'r--')
    #plt.subplot(312)
    plt.plot( nodes,inter_class_evolution_mean, 'g--')
    #plt.subplot(313)
    plt.plot( nodes,tot_inertia_evolution_mean, 'b--')
    plt.savefig(str(paths.__stock /('evolution-inertia-(intra,inter,tot)'+ description+'.jpg')))
    "save"
    print(datetime.now(), "stat end" )

    file = "result-"+ description +'.json'
       
    f = open(str(paths.__stock / file),'w')
    print(str(paths.__stock / file))
    json.dump(TT.tree_sup_stop(root),f, sort_keys=True, indent=4, separators=(',', ': '))
    f.close()
    
    
    leafFile = "leafs-"   + description +'.json'
    f = open(str(paths.__stock / leafFile),'w')   
    json.dump(leafs,f, sort_keys=True, indent=4, separators=(',', ': '))    
    f.close()
    """
    return (root,leafs)
    
    

"""second part, sub divise the leaf of the tree with a genre clustering
 (goal,it just use audio and video,no garanti that its a proper genre clustering) 
""" 
def genre_clustering(leafs,truth,ws,w_v,w_a,method ='spectral' ,min_var = 0.05 ): 
    vid_matrix = TT.normalization(list(w_v.values()))
    aud_matrix = TT.normalization(list(w_a.values()))
    
    cpt = 0
    w_video = {}
    w_audio = {}
    for k in w_v.keys():
         w_video[k] = vid_matrix[cpt]
         w_audio[k] = aud_matrix[cpt]
         
    new_leaf = []
    for leaf in leafs:
        vect = {}
        
        if len(leaf['videos']) > 5:
            for video in leaf['videos']:
                vect[video] = list(w_video[video]) + list(w_audio[video])
            childs,total,inter,intra = one_stepSimple(vect,method,min_var)
       
            leaf['inertia'] = {'total':total,'inter':inter,'intra':intra}
            for k in childs.keys():
                c = childs[k] 
                elem = {'id':leaf['id'] + str(k),'vid_vec':c,'children':[]}
                elem.update(TT.convert_to_read_(list(elem['vid_vec'].keys()),truth,ws,False))
                leaf['children'].append(elem)
                new_leaf.append(elem)

        
    
    return new_leaf     
   
    
"""testing the 2 step clustering"""    
def combinated_clustering():   
    kmeansContext = False
    min_var = 0.001
    method = 'spectral'
    
    vec,kv,t,w,ws,w_v,w_a,f_f = TT.separator( TT.load_file())
    vect =  {}
    for k,i in f_f.items():
        if i:
            if kmeansContext:
                vect[k] = kv[k][0]
            else:
                vect[k] = vec[k]
    description = 'subgender' +str(min_var)+ str(method)+"-"+ str(len(vec))   
    r ,l = thema_clustering(vect,t,ws,min_var )
    
    
    
    """inertia evolution"""
    intra,inter,total,nb_step = TT.inertia_evolution(r)
    nodes = [i for i in range(0,nb_step)]
    plt.figure(1,figsize=(10,5))
    #plt.subplot(311)
    plt.plot(nodes, intra, 'r--')
    #plt.subplot(312)
    plt.plot( nodes,inter, 'g--')
    #plt.subplot(313)
    plt.plot( nodes,total, 'b--')
    plt.savefig(str(paths.__stock /('evolution-inertia-(intra,inter,tot)'+ description+'.jpg')))
    
    
    
    file = "result-"+ description +'.json'
    print('before gender',TT.all_purity(r))
    nl = genre_clustering(l,t,ws,w_v,w_a,method =method ,min_var = min_var )
    
    """dendogram comutation"""
    dendo,lab = TT.tree_to_linkage(r)
    dendrogram(np.array(dendo,dtype  = 'double'),labels = lab)    
    plt.savefig(str(paths.__stock /('dendro-'+ description+'.jpg')) )
    
    print("after gender",TT.all_purity(r))
    
    "save"
    
    f = open(str(paths.__stock / file),'w')
    json.dump(TT.tree_sup_stop(r),f, sort_keys=True, indent=4, separators=(',', ': '))
    f.close()
    print(str(paths.__stock / file))
    
    leafFile = "leafs-" + description +'.json'
    lea = open(str(paths.__stock / leafFile),'w')   
    json.dump(nl,lea, sort_keys=True, indent=4, separators=(',', ': '))    
    lea.close()

if __name__ == '__main__':
    combinated_clustering()
    #complete_clustering(nb_step_max = 10,min_vid_clustering = 10,method = 'spectral',kmeansContext =False,one_united_vector = False)
    #new_complete_clustering(kmeansContext = False,t_weigth = 1,v_weigth = 1,a_weigth = 1)  
