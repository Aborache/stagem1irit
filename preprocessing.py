# -*- coding: utf-8 -*-
"""
Created on Mon Jun 17 09:52:06 2019

@author: Lenovo T420s
"""
import json
import TextExtractor as tx
import gensim
from Resources import paths
import csv

from WordEmbeding import meanContextProcessing as mC
from WordEmbeding import ContextProcessing as cProces


"""produce a corpus for the clustering phase where each vido have  vectors to represend text"""
"""don't forget to launch the ExtractENG to produce the list of videos before"""
def TotalPreprocess(listOfVideoNames):
    t = tx.extractGroundTruth()
    video_audio = tx.extractFeatures()
    dictionary,tags = tx.newVideoWordsmaker(listOfVideoNames)
    wordsEmb =  gensim.models.KeyedVectors.load_word2vec_format(paths.__temp_glove) 
    tfid = tx.weightsBuilder(dictionary,0.17)
    #video_vectors,videoSucc = mC.videosContext(dictionary, wordsEmb,tfid)
    video_vectors_kmeans,videoSucc = cProces.videosContext(dictionary, wordsEmb,tfid,2)
    video_vector_mean,video_succ2 = mC.videosContext(dictionary, wordsEmb,tfid)
    nameAndWordcontext = {}
    
    new_video = {}
    new_audio = {}
    for k,feat in video_audio.items():
        new_audio[k]= feat[:-5]
        new_video[k]= feat[-5:]

        
    new_truth = {}
    new_video_vectors = {}
    nameAndWordcontext = {}
    unit_model = {}
    for k,res in videoSucc.items():
        if res:
           new_truth[k[:-4]] =  t[k[:-4]]
           new_video_vectors[k[:-4]] = video_vectors_kmeans[k][0].tolist()
           nameAndWordcontext[k[:-4]] = cProces.ContextToWords(video_vectors_kmeans[k],wordsEmb)
           if video_succ2[k]:
               video_object = {}
               video_object["meanVector"] = video_vector_mean[k].tolist()
               video_object["KmeanVectors"] =[]
               video_object["f_audio"] = new_audio[k[:-4]]
               video_object["f_video"] = new_video[k[:-4]]
               video_object['features'] = True
               for elem in video_vectors_kmeans[k]:
                   video_object["KmeanVectors"].append(elem.tolist())
               video_object["meanWord"] =  mC.ContextToWords(video_vector_mean[k],wordsEmb)
               video_object["words"] =  cProces.ContextToWords(video_vectors_kmeans[k],wordsEmb)
               video_object["truth"] = t[k[:-4]]
               unit_model[k[:-4]] = video_object
               
           else:
               print(k,dictionary[k])
               
        else:
            print(k,dictionary[k])
          
    
    save_truth =  str(paths.__temp_file_file_to_truth)
    save_vect = str(paths.__temp_file_video_to_vec)
    save_words = str(paths.__temp_file_video_to_words)
    save_united = str(paths.__temp_file_video_to_united)
    
    ft = open(str(save_truth),'w')
    fv = open(str(save_vect),'w')
    fw = open(str(save_words),'w')
    fu = open(str(save_united),'w')
    
    print(len(dictionary),len(new_video_vectors))
    
    json.dump(new_truth,ft, sort_keys = True, indent=4, separators=(',', ': '))       
    json.dump(new_video_vectors,fv, sort_keys = True, indent=4, separators=(',', ': '))   
    json.dump(nameAndWordcontext,fw, sort_keys = True, indent=4, separators=(',', ': ')) 
    json.dump(unit_model,fu, sort_keys = True, indent=4, separators=(',', ': '))
    
    ft.close()
    fv.close()
    fw.close()
    fu.close()
    
    return 1
"""ad audio and video feature if presente, on the  file use as a corpus for clustering"""
def complet_Feature():
    save_united = str(paths.__temp_file_video_to_united)
    fu = open(str(save_united),'r')    
    u = json.load(fu)
    
    video_audio = tx.extractFeatures()
    new_video = {}
    new_audio = {}
    for k,feat in video_audio.items():
        new_audio[k]= feat[:-5]
        new_video[k]= feat[-5:]
        
    for k,e in u.items():
        if k in  new_audio.keys():
            e["f_audio"] = new_audio[k]
            e["f_video"] = new_video[k]
            e['features'] = True
        else:
            e['features'] = False
    fu = open(str(save_united),'w')        
    json.dump(u,fu, sort_keys = True, indent=4, separators=(',', ': '))    

def To_CSV():
    save_united = str(paths.__temp_file_video_to_united)
    fu = open(str(save_united),'r')    
    u = json.load(fu)
    
    csvfile =open('united.csv', 'w', newline='')
    fieldnames = ['name','truth','meanWord','contextwords','features','audio','video','meanvect', 'contextvects'] 
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()
    for k,e in u.items():
        if e['features']:
            a = e["f_audio"]
            v = e["f_video"]
        else:
            a = 0
            v = 0
        writer.writerow({'name': k,'truth':e["truth"],'meanWord':e["meanWord"],
                         'contextwords': e['words'],'features': e['features'],
                         'audio':a,'video':v,'meanvect':e["meanVector"],
                         'contextvects':e["KmeanVectors"]})

        

if __name__ == '__main__':
    #glove_file = datapath(str(vectorisorAdrGloveTwitter))    
    #tmp_file = get_tmpfile(str(tmpFile))
    #_ = glove2word2vec(glove_file, tmp_file)
    #TotalPreprocess(paths.ENGListFileName)
    complet_Feature()