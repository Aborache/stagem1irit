'''
Created on Jul 7, 2017

@author: Hasan Al Jawad
'''

from pathlib import Path

__PART__ = "DEV_"

__GENERAL__PATH__ = Path("c:\PartageGit\CodeStageM1\StageM1IRIT")

#path to the list of video names .xml, that is used for computing distance matrices and other tasks
__FINAL_DESCRIPTORS_LIST_XML__ = __GENERAL__PATH__ /  "Resources" / "finalDescriptorsList.txt"

#path to the list of video names .flv.ogv, that is used to generateLowLevelDescriptors
__FINAL_DESCRIPTORS_LIST_FLV_OGV__ =  __GENERAL__PATH__ / "listVideo.txt"


__PATH_TO_DEV_DESCRIPTORS_L0__ = __GENERAL__PATH__ / (__PART__ + "DESCRIPTORS_L0/")

#__PATH_TO_DEV_DESCRIPTORS_L1__ = __GENERAL__PATH__ + "/" + __PART__ + "DESCRIPTORS_L1/"
__PATH_TO_DEV_DESCRIPTORS_L1__ = __GENERAL__PATH__ / (__PART__ + "DESCRIPTORS_L1/")

__PATH_TO_DEV_AUDIO__ = __GENERAL__PATH__ / (__PART__ + "AUDIO/")

__PATH_TO_DEV_VIDEO__ = __GENERAL__PATH__ / (__PART__ + "VIDEO/")

__PATH_TO_DEV_METADATA__ = __GENERAL__PATH__ / (__PART__ + "METADATA/")

__PATH_TO_DEV_SHOTS__ = __GENERAL__PATH__ / (__PART__ + "SHOT/")

__PATH_TO_DEV_ASR__ = __GENERAL__PATH__  / (__PART__ + "LIMSI_ASR/")

__stock = __GENERAL__PATH__ / "WordEmbeding"
__temp_file_video_to_vec = __GENERAL__PATH__ / "temp" / "vid2vect.json"
__temp_file_video_to_united = __GENERAL__PATH__ / "temp" / "vid2united.json"
__temp_file_video_to_words = __GENERAL__PATH__ / "temp" / "vid2words.json"
__temp_file_file_to_truth = __GENERAL__PATH__ / "temp" / "vid2truth.json"
__temp_glove = __stock / 'glovConver.txt.'

ENGListFileName = __stock / "ENGVideoList.txt"
