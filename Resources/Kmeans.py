# -*- coding: utf-8 -*-
"""
Created on Mon May 20 16:04:21 2019

@author: Poquillon
"""

import numpy as np
import random as rd



class KMeans(object):
    def __init__(self, n_clusters:int, max_iter:int, early_stopping:bool=False,
                 tol:float=1e-4, display:bool=False) -> None:
        self.n_clusters = n_clusters            # Nombre de clusters
        self.max_iter = max_iter                # Nombre d'itération
        self.early_stopping = early_stopping    # arrête l'apprentissage si 
        self.tol = tol                          # seuil de tolérance entre 2 itérations
        self.display = display                  # affichage des données

        self.cluster_centers = None             # Coordonnées des centres de regroupement
                                                # (centre de gravité des classes)
    
    
    def _rand_cluster_center(self,X:np.ndarray):
        """ séléctionne aléatoirement les centroide de départ parmis les points de X"""
        rd.seed()
        self.cluster_centers = X[:self.n_clusters,:]
        for i in range(self.n_clusters):
            pos = rd.randint(0,X.shape[0] - 1)
            self.cluster_centers[i] = X[pos] 
                
        
        
    def _compute_distance(self, vec1:np.ndarray, vec2:np.ndarray) -> np.ndarray:
        """Retourne la distance quadratique entre vec1 et vec2 (squared euclidian distance)
        """		
        return np.sum(((vec1 - vec2) **2),axis = -1)
    
    def _compute_inertia(self, X:np.ndarray, y:np.ndarray) -> float:
        """Retourne la Sum of Squared Errors entre les points et le centre de leur
        cluster associe
        
        sum = 0.0
        
        for i in range(X.shape[0]):
            sum += self._compute_distance(X[i],self.cluster_centers[y[i]])
        return sum"""
        return np.sum(self._compute_distance(X,self.cluster_centers[y]))
    
    def _update_centers(self, X:np.ndarray, y:np.ndarray) -> None:
        """Recalcule les coordonnées des centres des clusters
        """
        self.cluster_centers = np.zeros(self.cluster_centers.shape) 
        """for i in range(self.cluster_centers.shape[0]):
            for j in range (n_dim):
                self.cluster_centers[i][j] = 0"""
        for i in range(self.n_clusters):
            tot = np.where(y == i,1,0)
            filtre = np.column_stack([tot,np.column_stack([tot,tot])])
            
            self.cluster_centers[i] = ( np.sum(X * filtre , axis = 0) / int(np.sum(tot))).astype(int)

        """for d in range(X.shape[0]):
            for f in range (n_dim):
                self.cluster_centers[y[d]][f] += X[d][f]
            self.cluster_centers[y[d]] += X[d]
            nb_byCluster[y[d]] += 1

        for i in range(self.n_clusters):
            for j in range (n_dim):
                if (nb_byCluster[i] > 0):
                    self.cluster_centers[i][j] = int(self.cluster_centers[i][j] / nb_byCluster[i])
        """
        pass
        """self.cluster_centers = np.where(nb_byCluster > 0, self.cluster_centers / nb_byCluster,self.cluster_centers)"""

    def predict(self, X:np.ndarray) -> np.ndarray:
        """attribue un indice de cluster à chaque point de data

        X = données
        y = cluster associé à chaque donnée
        """
        n_data = X.shape[0]
        y = np.zeros(n_data,dtype=int)
        
        distanceBase = self._compute_distance(X,np.resize(self.cluster_centers[0],(X.shape[0],X.shape[1])))
        for i in range(0,self.n_clusters):
            distanceCours = self._compute_distance(X,np.resize(self.cluster_centers[i],(X.shape[0],X.shape[1])))
            y = np.where(distanceCours<distanceBase,i,y)
            distanceBase = np.where(distanceCours<distanceBase,distanceCours,distanceBase)
        
        # nombre d'échantillons
        
        
		
        """for i in range(n_data):
            dist = (self._compute_distance(X[i],self.cluster_centers[0]))
            cluster = 0
			
            for j in range(self.n_clusters):
                distEnCours = self._compute_distance(X[i],self.cluster_centers[j])
                if dist > distEnCours :
                    dist = distEnCours
                    cluster = j
            y[i] = cluster"""
        return y
        pass

    def fit(self, X:np.ndarray) -> None:
        """Apprentissage des centroides
        """
        # Récupère le nombre de données
        n_data = X.shape[0]

        # Sauvegarde tous les calculs de la somme des distances euclidiennes pour l'affichage
        if self.display:
            shutil.rmtree('./img_training', ignore_errors=True)
            metric = []

        # 2 cas à traiter : 
        #   - soit le nombre de clusters est supérieur ou égale au nombre de données
        #   - soit le nombre de clusters est inférieur au nombre de données
        if self.n_clusters >= n_data:
            # Initialisation des centroides : chacune des données est le centre d'un clusteur
            self.cluster_centers = np.zeros(self.n_clusters, X.shape[1])
            self.cluster_centers[:n_data] = X
        else:
            # Initialisation des centroides
            #self.cluster_centers = X[:self.n_clusters,:]
            self._rand_cluster_center(X)
            # initialisation d'un paramètre permettant de stopper les itérations lors de la convergence
            stabilise = False

            # Exécution de l'algorithme sur plusieurs itérations
            for i in range(self.max_iter):
                # détermine le numéro du cluster pour chacune de nos données
                y = self.predict(X)

                # calcule de la somme des distances initialiser le paramètres
                # de la somme des distances
                if i == 0:
                    current_distance = self._compute_inertia(X, y)

                # mise à jour des centroides
                self._update_centers(X, y)

                # mise à jour de la somme des distances
                old_distance = current_distance
                current_distance = self._compute_inertia(X, y)

                # stoppe l'algorithme si la somme des distances quadratiques entre 
                # 2 itérations est inférieur au seuil de tolérance
                if self.early_stopping:
                    # A compléter
                    stabilise = abs(current_distance - old_distance) < self.tol
                    if stabilise:
                        break

                # affichage des clusters


    def score(self, X:np.ndarray, y:np.ndarray) -> float:
        """Calcule le score de pureté
        """
        n_data = X.shape[0]

        y_pred = self.predict(X)

        score = 0
        for i in range(self.n_clusters):
            _, counts = np.unique(y[y_pred == i], return_counts=True) 
            score += counts.max()

        score /= n_data

        return score