'''
Created on Sep 21, 2016

@author: root
'''
def returnVideoTypeList(filename):
    videoTypeDict = {}
    infile = open(filename,"r")
    for line in infile:
        lineWords = line.split("\t")
        videoTypeDict[lineWords[0]] = lineWords[1]
    infile.close()
    return videoTypeDict
def attachGroundTructhtoClusters(clusterFileName, filename):
    videoTypeDict = returnVideoTypeList(filename)
    infile = open(clusterFileName,"r")
    outfile = open("ResultsSCwithTypes.txt","w")
    for line in infile:
        if line=="\n" or line.startswith("Classe"):
            outfile.write(line)
        else:
            type = videoTypeDict[line[:-9]]
            print(line[:-9] + " , "+ type)
            name = line[:-1]
            outfile.write(name)
            outfile.write("\t")
            outfile.write(type)
            outfile.write("\n")
    outfile.close()
    infile.close()
def computeDistributionPerClusters(clusterFileName, filename):
    videoTypeDict = returnVideoTypeList(filename)
    listvideos=[]
    infile = open(clusterFileName,"r")
    outfile = open("ResultsSCwithTypesDistributionperClusters.txt","w")
    for line in infile:
        if line=="\n" or line.startswith("Classe"):
            outfile.write(line)
            dist = computeAndWriteDistribution(listvideos,outfile)
            listvideos=[]
        else:
            type = videoTypeDict[line[:-9]]
            listvideos.append(type)
    outfile.close()
    infile.close()
def computeAndWriteDistribution(listvideos,outfile):
    if listvideos==[]:
        return
    listvideos.sort()
    dict = {}
    for v in listvideos:
        if v in dict:
            dict[v]=dict[v]+1
        else:
            dict[v]=1
    for key in dict:
        outfile.write(key + "\t"+str(dict[key])+"\n")
        
attachGroundTructhtoClusters("ResultatSC.txt", "IndexedVideoList.txt")
computeDistributionPerClusters("ResultatSC.txt", "IndexedVideoList.txt")