'''
Created on Jun 10, 2017

@author: Hasan Al Jawad
'''
import prettify
from xml.etree.cElementTree import Element, SubElement

from scipy.cluster.hierarchy import linkage, dendrogram, cophenet, fcluster
from scipy.spatial.distance import pdist
from Resources import paths

import matplotlib.pyplot as plt
import numpy as np

def fancy_dendrogram(*args, **kwargs):
    title=kwargs.pop('title', 'Hierarchical Clustering (truncated)')
    max_d = kwargs.pop('max_d', None)
    if max_d and 'color_threshold' not in kwargs:
        kwargs['color_threshold'] = max_d
    annotate_above = kwargs.pop('annotate_above', 0)

    ddata = dendrogram(*args, **kwargs)

    if not kwargs.get('no_plot', False):
        plt.title(title)
        plt.xlabel('sample index or (cluster size)')
        plt.ylabel('centroid distance')
        for i, d, c in zip(ddata['icoord'], ddata['dcoord'], ddata['color_list']):
            x = 0.5 * sum(i[1:3])
            y = d[1]
            if y > annotate_above:
                plt.plot(x, y, 'o', c=c)
                plt.annotate("%.3g" % y, (x, y), xytext=(0, -5),
                             textcoords='offset points',
                             va='top', ha='center')
        if max_d:
            plt.axhline(y=max_d, c='k')
    return ddata

def execute():
    f=open(paths.__FINAL_DESCRIPTORS_LIST_XML__, 'r')
    file_names = f.read().splitlines()
    f.close()
    
    data = np.genfromtxt("DistanceMatrix_nbSegments2.txt",dtype=float, delimiter=",")

    data_link = linkage(data, 'centroid') # computing the linkage
    max_d=2000
    
   
    
    plt.figure(figsize=(12, 5))
    fancy_dendrogram(
        data_link,
        truncate_mode='lastp',  # show only the last p merged clusters
        p=100,  # show only the last p merged clusters
        leaf_rotation=90.,  # rotates the x axis labels
        leaf_font_size=8.,  # font size for the x axis labels
        show_contracted=True,  # to get a distribution impression in truncated
        annotate_above=max_d,    # useful in small plots so annotations don't overlap
        max_d=max_d,  # plot a horizontal cut-off line
        title='Hierarchical Clustering - ',
    )
    plt.show()
    
    clusters = fcluster(data_link, max_d, criterion='distance')
    
    video_cluster_map = {}
    for i in range(1, max(clusters)+1):
        video_cluster_map[i] = []
        
    for index in range(len(clusters)):
        video_cluster_map[clusters[index]] += [file_names[index]]
    
    root = Element('RunFile')
    categories = SubElement(root, 'Categories')
    clusters_e = SubElement(root , 'Clusters')
    clusters_e.set('nb', str(max(clusters)))
    
    for key in video_cluster_map:
        cluster = SubElement(clusters_e, 'Cluster')
        cluster.set('ClusterID', str(key))
        cluster.set('NbVideo', str(len(video_cluster_map[key])))
    
        listVideo = SubElement(cluster, 'ListVideo')
        
        for value in video_cluster_map[key]:
            video = SubElement(listVideo, 'Video')
            video.text = value
            
    
    filename='Run_HC_2Segments.xml'
    f=open(filename, 'w')
    f.write(prettify.prettify(root))
    f.close()
    
execute()
