'''
Created on Jun 28, 2017

@author: Hasan AL Jawad
'''
from MultiLevelSimilarity import cosineSimilarity
import numpy as np
import queue
import threading
import logging

logging.basicConfig(level=logging.DEBUG,
                    format='[%(levelname)s] (%(threadName)-10s) %(message)s',
                    )

"""
@Multithreading section start

in_q = Queue.Queue()
out_q = Queue.Queue()
nb_threads = 8

        
def call_SimilarityMeasure(in_q, out_q):
    '''This is the worker thread function.
    It processes items in the queue one after
    another. Those items are pair of segments to compare.
    These daemon threads go into an
    infinite loop, and only exit when
    the main thread ends.
    '''
    while True:
        #print 'Looking for the next pair'
        pair = in_q.get()
        #logging.debug("Initiated on pair of segments")
        
        #text similarity
        #dist_value = cosineSimilarity.get_cosine(cosineSimilarity.text_to_vector(pair[0]), 
        #                                                 cosineSimilarity.text_to_vector(pair[1]))
        
        #audio video similarity
        dist_value = euclidean(pair[0], pair[1])
        
        
        out_q.put(dist_value)
        #logging.debug("cos between segments: " + str(dist_value))
        in_q.task_done()
  
for i in range(nb_threads):
    th = threading.Thread(target=call_SimilarityMeasure, args=(in_q, out_q))
    th.setDaemon(True)
    th.start()


@Multithreading section end
"""
#kmeans-jaccard-id
#weights=[0.0441815434023,0.0427883253046,0.0397421365826,0.0411353546803,0.0414659488051,0.0489751582129,0.0506517427033,0.0451024841787,0.0433550580901,0.0409700576178,0.0442287711344,0.0409936714839,0.0421035231888,0.0423868895816,0.0432133748937,0.0452913951072,0.0455511476339,0.0437801076792,0.0432133748937,0.0425994143761,0.0456692169642,0.0391990176632,0.0416666666667,0.0434022858222]

#kmeans-intersection-id
weights=[0.0434466879207,0.0574256110521,0.0620040736805,0.0455410910379,0.0387221041445,0.0407678002125,0.0813894792774,0.0888903648601,0.0339488133192,0.0353613177471,0.0312212185618,0.0416666666667,0.033705278073,0.033705278073,0.0310263903649,0.0312699256111,0.0317569961034,0.0315134608572,0.0310263903649,0.0322440665958,0.0345820049593,0.0385272759476,0.0416666666667,0.0285910379029]

#kmeans-jaccard-category
#weights=[0.0386134606259,0.0434881221562,0.0475567372917,0.0422982441448,0.0409548334869,0.0389589090808,0.049974876476,0.0550030706529,0.0388821427574,0.041837646205,0.0375771152611,0.0416666666667,0.0447931496525,0.0411083661335,0.0380760963627,0.0391124417274,0.0376538815845,0.038268012171,0.0391124417274,0.0398801049605,0.0409932166485,0.0442941685509,0.0416666666667,0.0382296290093]

#kmeans-intersection-category
#weights=[0.0456878484516,0.0423864607496,0.0410435233793,0.0399803646278,0.0435615309486,0.0448485125951,0.0412113905506,0.045352114109,0.0402881211085,0.0377980914011,0.0409036340699,0.0416666666667,0.0431138851585,0.039700586009,0.0389731616001,0.0397565417328,0.0399803646278,0.0400083424897,0.0402041875229,0.0435895088105,0.0432537744679,0.0442889553575,0.0416666666667,0.0407357668986]
def euclidean(x,y):
    sumSq=0.0
    
    #add up the squared differences
    for i in range(len(x)):
        sumSq+=(x[i]-y[i])**2
        
    #take the square root of the result
    return (sumSq**0.5)

def euclidean_weighted(x,y, w):
    sumSq=0.0
    
    #add up the squared differences
    for i in range(len(x)):
        sumSq+= ((x[i]-y[i])**2)*w[i]
        
    #take the square root of the result
    return (sumSq**0.5)

def compute_distance(s1, s2):
    """
    hausdorff algorithm
    """
    infimum_array=[]
    
    for item1 in s1:
        temp_dist_array = []
        for item2 in s2:
            temp_dist_array.append(euclidean(item1, item2))
        infimum_array.append(min(temp_dist_array))
        
    return max(infimum_array)

def hausdorff_distance(subset1, subset2):
    """
    calculates the hausdorff distance between two subsets
    @param subset1 subset2: 2-dim arrays
    """
    if len(subset1) == 1:
        d1 = compute_distance(subset1, subset2);
        return d1
    elif len(subset2) == 1:
        d1 = compute_distance(subset2, subset1);
        return d1
    else:
        d1 = compute_distance(subset1, subset2);
        d2 = compute_distance(subset2, subset1);
        return max(d1, d2)
  
  
def mean_distance(subset1, subset2):
    if len(subset1) != len(subset2):
        return None
    sum_dist=0.0
    for index in range(len(subset1)):
        sum_dist += euclidean(subset1[index], subset2[index])
    return sum_dist / len(subset1)

def matrix_distance(subset1, subset2):
    
    
    
    
    matrix = []
    for s1 in range(len(subset1)):
        row = []
        for s2 in range(len(subset2)):
            dist_value = 0.0
            if subset1[s1] and subset2[s2]:   
                #in_q.put((subset1[s1],subset2[s2]))
                
                #considering individual feature weights
                dist_value = euclidean(subset1[s1], subset2[s2])
			
			# considering individual feature weights
			#dist_value = euclidean_weighted(subset1[s1], subset2[s2], weights)
			
			# considering modality weights
                dist_value_v = euclidean(subset1[s1][19:], subset2[s2][19:])
                dist_value_a = euclidean(subset1[s1][:18], subset2[s2][:18])
                dist_value = dist_value_a * 0.75 + dist_value_v * 0.25
                
                
                
                #dist_value = cosineSimilarity.get_cosine(cosineSimilarity.text_to_vector(subset1[s1]), 
                #                                         cosineSimilarity.text_to_vector(subset2[s2]))
            else:
                #out_q.put(dist_value)
                row.append(dist_value)
                
            #in_q.join()
            #row = [out_q.get(i) for i in range(out_q.qsize())]
            row.append(dist_value)
            
        matrix.append([np.min(row)])
        
    np_matrix = np.array(matrix)
    
    #return np_matrix

    #Get the mean of rows
    rows_mean = np_matrix.mean(axis=1)
    return np.sum(rows_mean)/(len(rows_mean)*1.0)
    
     
#dv1=[[0.0,0.0,0.0,0.0,0.5,84.59,0.0,0.0,0.5,0.0,84.59,100.0,0.558659217877,25.41,54.64,13.27,11.35,41.37,32.45,0,0,6.14525139665,0,55.4444444444]]
#dv2=[[11.78,7.61,12.78,43.08,4.24,1.89,0.0,0.37,0.0,0.0,78.05,100.0,6.41562064156,36.53,85.9,31.58,4.75,54.32,6.88,0,0,3.20781032078,0,43.2222222222]]
#print hausdorff_distance(dv1, dv2)