'''
Created on Jun 29, 2017

@author: Hasan AL Jawad
'''
from tkinter.tix import ROW
from lxml import etree
from multiprocessing import Process, Manager
from numpy import array, savetxt, sum
import os
import sys
import time
import queue
import traceback
import threading
import logging
from . import plot

from MultiLevelSimilarity import convertToXML
from Resources import paths
from . import distanceMetric as dist
import numpy as np

logging.basicConfig(level=logging.DEBUG,
                    format='[%(levelname)s] (%(processName)s) (%(threadName)-10s) %(message)s',
                    )

__PATH_DESCRIPTORS_LIST__ = os.getcwd() + "/DescriptorsList.xml"
__DELIMITER__ = ","

"""
@Multithreading section start
"""

def call_dist_matrix_distance(in_q, out_q):
    """This is the worker thread function.
    It processes items in the queue one after
    another. Those items are pair of segments to compare.
    These daemon threads go into an
    infinite loop, and only exit when
    the main thread ends.
    """
    while True:
        #print 'Looking for the next pair'
        pair = in_q.get()
        #logging.debug("Initiated on pair of levels")
        
        dist_value = dist.matrix_distance(pair[0], pair[1])
        out_q.put(dist_value)
        #logging.debug("cos between levels: " + str(dist_value))
        
        in_q.task_done()
        


"""
@Multithreading section end
"""

        
def writeMatrixToFile(matrix, filename):
    
    #np_matrix = np_matrix.transpose() + np_matrix
    f = open(filename, 'ab')
    savetxt(f, matrix, delimiter=",", fmt="%s")
    f.close()
    logging.debug("Appended to file successfully !")

def extractDescriptorVector(element):
    desc_vector = []
    for dv in element.findall("Segment"):
        splitTxt = dv.text[:-1].split(__DELIMITER__)
        temp_array=[]
        for num in splitTxt:
            temp_array.append(float(num))
        desc_vector.append(temp_array)
    return desc_vector

def extractTranscriptVector(element):
    trans_vector = []
    for dv in element.findall("Transcript"):
        trans_vector.append(dv.text)
    return trans_vector

def compare2levels(lvl1, lvl2):
    dv_1 = extractDescriptorVector(lvl1)
    dv_2 = extractDescriptorVector(lvl2)
            
    return dist.matrix_distance(dv_1, dv_2)

def compare2videos(video_desc1, video_desc2, in_q, out_q):
    matrix = []
    
    if len(video_desc1) < len(video_desc2):
        temp = video_desc1
        video_desc1 = video_desc2
        video_desc2 = temp
         
    for desc_level_1 in video_desc1:
        row = []
        
        '''Descriptors/Features'''
        dv_1 = extractDescriptorVector(desc_level_1)
        
        for desc_level_2 in video_desc2:
            
            '''
            Descriptors/Features
            '''
            dv_2 = extractDescriptorVector(desc_level_2)
            
            '''
            Transcript text data
            '''
            #txtv_1 = extractTranscriptVector(desc_level_1)
            #txtv_2 = extractTranscriptVector(desc_level_2)
            
            '''
            Instead of appending directly to the list "row", put vector pairs inside a queue to be 
            processed by threads in parallel
            '''
            in_q.put((dv_1, dv_2))
            #row.append(dist.matrix_distance(dv_1, dv_2))
        
        '''
        Here, in_q.join(), waits for the working threads to finish and put their results in out_q
        '''    
        in_q.join()
        row = [out_q.get(i) for i in range(out_q.qsize())]
            
        matrix.append([np.min(row)])
        
    np_matrix = array(matrix)
    
    #return np_matrix
    
    rows_mean = np_matrix.mean(axis=1)
    return sum(rows_mean)/(len(rows_mean)*1.0)

def run_fixed_segments(pathToDescriptorsList, nbSegments, distanceMetric):
    if os.path.exists(pathToDescriptorsList):
        try:
            videosList = etree.parse(pathToDescriptorsList).getroot().findall("Video")
        except:
            traceback.print_exc()
            sys.exit()
    else:
        print("File " + pathToDescriptorsList + " doesn't exist")
        sys.exit()
    #print len(videosList)
    matrix = []    
    for i in range(len(videosList)):
        print(str(i))
        row = []
        for j in range(len(videosList)):
            if i < j:
                
                decrement = 1
                
                while True:
                    try:
                        video1 = videosList[i][nbSegments - decrement]
                        break
                    except:
                        decrement += 1
                        if decrement > nbSegments:
                            print("Some videos dont have descriptors. Video name : " + videosList[i].get("name"))
                            sys.exit()
                
                while True:
                    try:
                        video2 = videosList[j][nbSegments - decrement]
                        break
                    except:
                        decrement += 1
                        if decrement > nbSegments:
                            print("Some videos dont have descriptors. Video name : " + videosList[i].get("name"))
                            sys.exit()
                
                dv_1 = extractDescriptorVector(video1)
                dv_2 = extractDescriptorVector(video2)
                
                try:    
                    if distanceMetric == "hausdorff":
                        distanceValue = dist.hausdorff_distance(dv_1, dv_2)
                    elif distanceMetric == "mean":
                        distanceValue = dist.mean_distance(dv_1, dv_2)
                    elif distanceMetric == "matrix":
                        distanceValue = dist.matrix_distance(dv_1, dv_2)
                except:
                    row.append(0.00)
                    continue
                    
                row.append(distanceValue)
            else:
                row.append(0.00)
        
        matrix.append(row)
    
    
    np_matrix = array(matrix, dtype=float)
    writeMatrixToFile(np_matrix, "DistanceMatrix_Videos_mean.txt")

def run_dynamic_segments(pathToDescriptorsList, distanceMetric, outputFile, start_index, end_index):
    if os.path.exists(pathToDescriptorsList):
        try:
            videosList = etree.parse(pathToDescriptorsList).getroot().findall("Video")
        except:
            traceback.print_exc()
            sys.exit()
    else:
        print("File " + pathToDescriptorsList + " doesn't exist")
        sys.exit()
        
    in_q = queue.Queue()
    out_q = queue.Queue()
    nb_threads = 8

    for i in range(nb_threads):
        th = threading.Thread(target=call_dist_matrix_distance, args=(in_q, out_q))
        th.setDaemon(True)
        th.start()

    for i in range(len(videosList)):
        if i >= start_index and i < end_index:
            logging.debug("Starting with : " + str(i) + " | " + videosList[i].get("name"))
            comparison_matrix = [] 
            row=[]
            for j in range(len(videosList)):
                if i <= j:
                    #logging.debug("Video1 = " + str(i) + "; Video2 = " + str(j))
                    row.append(round(compare2videos(videosList[i], videosList[j], in_q, out_q), 3))
                else:
                    row.append(0.0)
                    
            comparison_matrix.append(row)  
            writeMatrixToFile(comparison_matrix, outputFile)
    
def split_data(path_to_data_file ,nb_proc):
    if os.path.exists(path_to_data_file):
        with open(path_to_data_file, 'r') as f:
            lines = f.read().splitlines()
            index_lines = {}
            for index, line in enumerate(lines):
                index_lines[index] = line
            print(lines[:10])
            print(len(lines))
            nb_videos_per_part = len(lines)/ nb_proc
            print(nb_videos_per_part)
            sub_index = 0
            
            for proc in range(nb_proc):
                with open(os.getcwd() + '/splitData/P_' + str(proc) + '.txt', 'w') as proc_file:
                    for i in range(sub_index, sub_index + nb_videos_per_part):
                        if i < len(lines):
                            #proc_file.write(index_lines[i] + '\n')
                            proc_file.write("Start = " +  + '\n')
                        else:
                            return
                        
                    if proc == nb_proc-1:
                        remainder = len(lines) - (nb_videos_per_part * nb_proc)
                        sub_index += nb_videos_per_part
                        for i in range(sub_index,sub_index + remainder):
                            proc_file.write(index_lines[i] + '\n')
                    
                    convertToXML.toXML(os.path.realpath(f.name)
                                       , paths.__PATH_TO_DEV_DESCRIPTORS_L1__
                                       , os.path.realpath(f.name)[:-4] + '.xml')
                    
                sub_index += nb_videos_per_part
    else:
        print("NO")
    
         
if __name__ == '__main__':
    """
    @Multiprocess: Start
     
    nb_proc = 8
    proc_list = []
    
    videosList_all = etree.parse("/home/hassan/workspace/VideoClustering.v0/MultiLevelSimilarity/Temp_DescriptorsList.xml").getroot().findall("Video")
    total_len = len(videosList_all)
    nb_videos_per_part = total_len/nb_proc
    
    f=open("distribution.txt", 'w')
    for proc in range(nb_proc):
        if proc == nb_proc - 1:
            start_index = proc * nb_videos_per_part
            end_index = start_index + nb_videos_per_part + (total_len - (nb_videos_per_part * nb_proc))
        else:
            start_index = proc * nb_videos_per_part
            end_index = start_index + nb_videos_per_part
        
        f.write("proc " + str(proc) + " start " + str(start_index) + " end " + str(end_index) + "\n")
    f.close()     
        #procInstance = Process(target=run_dynamic_segments, args=("/home/hassan/workspace/VideoClustering.v0/MultiLevelSimilarity/Temp_DescriptorsList.xml"
        #                                                          , "matrix"
        #                                                          , os.getcwd() + \
        #                                                          "/splitResult/P_" + str(proc) + ".txt"
        #                                                          ,start_index
        #                                                          ,end_index))
        #proc_list.append(procInstance)
        #procInstance.start()
        
    #for proc in proc_list:
    #    proc.join()  
         
    
    @Multiprocess: end
    """
    
    
    run_dynamic_segments("/home/hassan/workspace/VideoClustering.v0/MultiLevelSimilarity/DescriptorsList.xml", 
                         "matrix", "dist_matrix_audio75_video25.txt", 0, 4049)
    