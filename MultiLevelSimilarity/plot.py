import matplotlib.pyplot as plt
import numpy as np
from random import random
from numpy import array
import os

def print_matrix_fromText(textFile, featruematrix=False):
    if featruematrix == False:
        a = np.genfromtxt(textFile,dtype=float, delimiter=",")
        a = a.transpose() + a
        plt.imshow(a, cmap='viridis', interpolation='nearest')
        plt.colorbar()
        plt.show()
    elif featruematrix == True:
        with open(textFile, 'r') as f:
            feature_names = f.readline().split(',')[1:]
            
        a = np.genfromtxt(textFile,dtype=float, delimiter=",", skip_header=1, usecols=tuple(range(1, 23)))
        a = a.transpose() + a
        #a=1-a
        np.fill_diagonal(a, 0.)
        ax = plt.imshow(a, cmap='viridis', interpolation='nearest', vmin=0.0, vmax=1.0)
        
        plt.xticks(np.arange(0, a.shape[1], 1.0), feature_names)
        plt.yticks(np.arange(0, a.shape[1], 1.0), feature_names)
        plt.xticks(rotation=90)
        plt.title(os.path.basename(textFile)[:-4])
        plt.colorbar()
        plt.show()
        #plt.savefig(os.path.basename(textFile)[:-4] + '.png')
        #plt.close()
        

def print_matrix(matrix, lvl1, lvl2):
    fig = plt.figure()
    ax = fig.add_subplot(111)
    
    median = np.median(matrix)
    img = ax.imshow(matrix, vmin=0, vmax=250, cmap='viridis', interpolation='nearest')
    
    for j in range(matrix.shape[0]): 
        for i in range(matrix.shape[1]): 
            if matrix[j,i] > median:
                color_text = 'black'
            else:
                color_text = 'white'
            ax.text(i,j,round(matrix[j,i],2), ha='center', va='center', color=color_text)
    
    plt.xticks(np.arange(0, matrix.shape[1], 1.0))
    plt.yticks(np.arange(0, matrix.shape[0], 1.0))
    plt.xlabel("Video2")
    plt.ylabel("Video1")
    plt.title("Matrix between V1-level-" + str(lvl1) + " & V2-level-" + str(lvl2))
    plt.colorbar(img)
    filename = "segment_matrix_lvl_" + str(lvl1) + "_" + str(lvl2) + ".png"
    plt.savefig(filename)
    plt.close()
    #plt.show()
 
def saveDoubleMatrix(matrix):
    for row in range(matrix.shape[0]):
        for col in range(matrix.shape[1]):
            print_matrix(array(matrix[row, col]), row+1, col+1) 
    
if __name__ == '__main__':
    #print_matrix_fromText("/home/hassan/workspace/VideoClustering.v0/MultiLevelSimilarity/dist_matrix_mean.txt")
    #exit()
    print_matrix_fromText('/media/hassan/KINGSTON/result 17-8/Jaccard Matching/FeatureIntersectionMatrix_kmeans_jaccard_byId.csv', featruematrix=True)
    exit()
    
    dir = '/media/hassan/KINGSTON/result 17-8/Intersection Matching'
    filenames = os.listdir(dir)
    for filename in filenames:
        print_matrix_fromText(dir + '/' +filename, featruematrix=True) 
    
    #print_matrix_fromText('/media/hassan/KINGSTON/result 17-8/Intersection Matching'
    #                      , featruematrix=True)