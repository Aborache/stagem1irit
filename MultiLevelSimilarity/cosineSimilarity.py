'''
Created on Jul 16, 2017

@author: TOSHIBA
'''
import re, math
from collections import Counter,defaultdict
from nltk.corpus import wordnet
from nltk.metrics import edit_distance
from itertools import product
from nltk.tokenize import word_tokenize

WORD = re.compile(r'\w+')
DIGIT = re.compile(r'\d')

def wordnet_Similarity(word1, word2, allsynsets):
    return wordnet.wup_similarity(allsynsets[word1][0], allsynsets[word2][0])

def levenshtein_similarity(word1, word2):
    return edit_distance(word1, word2)

def get_cosine(vec1, vec2):
    intersection = set(vec1.keys()) & set(vec2.keys())
    numerator = sum([vec1[x] * vec2[x] for x in intersection])

    sum1 = sum([vec1[x]**2 for x in list(vec1.keys())])
    sum2 = sum([vec2[x]**2 for x in list(vec2.keys())])
    denominator = math.sqrt(sum1) * math.sqrt(sum2)

    if not denominator:
        return 0.0
    else:
        return float(numerator) / denominator

def get_soft_cosine(vec1, vec2, distance='wordnet'):
    
    word_intersection = set(vec1.keys()) | set(vec2.keys())
    length_vec1 = len(list(vec1.keys()))
    length_vec2 = len(list(vec2.keys()))
    print(word_intersection)
    
    temp_vec_1 = defaultdict(int)
    temp_vec_2 = defaultdict(int)
    for word in word_intersection:
        if word in list(vec1.keys()):
            temp_vec_1[word] = (vec1[word])
        else:
            temp_vec_1[word] = 0
            
        if word in list(vec2.keys()):
            temp_vec_2[word] = (vec2[word])
        else:
            temp_vec_2[word] = 0
    vec1 = temp_vec_1; vec2 = temp_vec_2
    print(vec1);print(vec2);print("length of vec1 = " + str(length_vec1));print("length of vec2 = " + str(length_vec2));
    
    similarity_dict={};allsynsets={};numerator = 0.0;sum1=0.0;sum2=0.0
    
    if distance == 'wordnet':
        #create dictionary of all synsets
        for word in word_intersection:
            allsynsets[word] = wordnet.synsets(word)
    
    #create dictionary of similarity between all pairs of words
    for word1, word2 in product(word_intersection, word_intersection):
        tuple_key = (word1, word2)
        try:
            if distance == 'wordnet':
                similarity_value = wordnet_Similarity(word1, word2, allsynsets)
            elif distance == 'levenshtein':
                similarity_value = levenshtein_similarity(word1, word2)
                
            similarity_dict[tuple_key] = similarity_value
        except:
            similarity_dict[tuple_key] = None
    
    for i, j in product(word_intersection, word_intersection):
        try:
            similarity = similarity_dict[(i, j)]
            
            if similarity:
                
                numerator += numerator/100 + (similarity * vec1[i]/length_vec1 * vec2[j]/length_vec2)
                
                sum1 += sum1/100 + (similarity * vec1[i]/length_vec1 * vec1[j]/length_vec1)
                sum2 += sum2/100 + (similarity * vec2[i]/length_vec2 * vec2[j]/length_vec2)
        except:
            pass
    
    denominator = math.sqrt(sum1) * math.sqrt(sum2)
    
    if not denominator:
        return 0.0
    else:
        return float(numerator) / denominator


def text_to_vector(text):
    words = []
    if text:
        
        #tokenize the string of words
        #words = WORD.findall(text)
        words = word_tokenize(text)
        
        #Remove words that contain one letter or digits
        words = [word for word in words if len(word) > 1 and len(DIGIT.findall(word)) == 0]
        
        #lemmatize the words
        
        
    return Counter(words)
