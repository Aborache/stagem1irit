# -*- coding: utf-8 -*-
"""
Created on Wed May 15 15:13:21 2019

@author: Mathieu Poquillon
"""

from Resources import paths
import xml.etree.ElementTree as ET
import os
import sys
import gensim
import logging
import numpy
import TextExtractor as tx

from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from threading import Thread

l0 = paths.__PATH_TO_DEV_DESCRIPTORS_L0__
stock = paths.__stock
formatedTrainimgText = str(stock / "learningData.txt")
vectorisorAdr = str(stock / "wordEm.vector")
modelAdr = str(stock / "wordEm.model")
listOfVideoNames = paths.__FINAL_DESCRIPTORS_LIST_XML__

vector_dim = 200
# temporal cut



def Filter(sentence,filters):
    """ from a string sentence while return the 
    sentence less the words that are catagorised as
    stop word in the filters language
    """
    wordsFiltered = ""
    words = word_tokenize(sentence)
    stopWords = set(stopwords.words(filters))
    for w in words:
        if w not in stopWords:
            wordsFiltered += str(w) + " "
    return wordsFiltered

def MeanVectorCalculator(words,vectorisor):
    matrix = numpy.zeros((1,vector_dim))
    cpt = 0
    z = numpy.zeros((1,vector_dim))
    for word in words:  
        try:
            m = vectorisor.get_vector(word)
            i = 1
        except KeyError as e:
            f = open("error.txt",'a')
            f.write("vocab prob:" + str(e))
            m = z
            i = 0
        matrix += m
        cpt += i
    if (cpt >= 1):
        return (matrix / cpt)
        
    else:
        return numpy.zeros((1,vector_dim))
    
def meanVectorCalculator2(words,vectorisor):
    newWords = set(vectorisor.index2entity).intersection(words)
    if len(newWords)> 0:
        return (numpy.mean(numpy.array(vectorisor[newWords]),axis=0)) 
    else:
        return numpy.zeros((1,vector_dim))
    
def meanVectorCalculatorWeight(words,vectorisor,weight):
    """ il faut normlaliser par rapport aux poids""" 
    newWords = set(vectorisor.index2entity).intersection(words)
    newWordsWeight =(numpy.array( [weight[w] for w in newWords])).reshape((len(newWords),1))
    
    if len(newWords)> 0:
        return (numpy.mean( numpy.array(vectorisor[newWords] ) *  newWordsWeight,axis=0) / numpy.sum(newWordsWeight) ) 
    else:
        return numpy.zeros((1,vector_dim))
 

def extractTextENGL(inputFileXML,filterLang= 'english'):
    """ add to the formatedTrainimgText the imputFileXML (english) transcript 
    """
    print('begin')
    filt =stopwords.words(filterLang) + ['{fw}','.',',']
    res = tx.extractorL0File(inputFileXML,filt)
    f = open(formatedTrainimgText,'a')
    for line in res:
        sent =''
        for word in line:
            sent += word + ' '
        sent += '\n'
        f.write(sent)
    f.close()

def generateWord2vectorconv():
    """ base on the data store in formatedTrainimgText the process while
    generate the wordEmbeding and the map that while associate words
    with there own context vector 
    """
    sentences = gensim.models.word2vec.LineSentence(formatedTrainimgText, max_sentence_length=10000, limit=None)
    print('reading sentence')
    logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
    model = gensim.models.Word2Vec(sentences, iter=10, min_count=10, size=vector_dim, workers=4)
    
    # get the most common words for control checking
    print(model.wv.index2word[0], model.wv.index2word[1], model.wv.index2word[2],model.wv.index2word[3], model.wv.index2word[4], model.wv.index2word[5])

    model.save(modelAdr)
    word_vectors = model.wv
    word_vectors.save(vectorisorAdr)
  
    
def extractorVector(listeOfVideo,wv = gensim.models.KeyedVectors.load(vectorisorAdr, mmap='r')):
    if os.path.exists(listOfVideoNames):
        try:
            f = open(listOfVideoNames, 'r')
            file_names = f.read().splitlines()
            f.close()
        except IOError as e:
            print("Could not read file:", listOfVideoNames)
            print(str(e))
            sys.exit()
    else:
        print("File  " + str(listOfVideoNames) + " doesn't exist")
        sys.exit()
    fileToMeanVect=[]
    for line in file_names:
        imputF = str(paths.__PATH_TO_DEV_DESCRIPTORS_L0__ / line)
        res = tx.extractorL0File(imputF,[])
        words = []
        for sent in res:
            words.extend(sent)
        m = str(MeanVectorCalculator(words,wv))[0]
        fileToMeanVect.insert(line, m)
    return(fileToMeanVect)
        
    
    
def addvectors(inputFileXML,wv = gensim.models.KeyedVectors.load(vectorisorAdr, mmap='r') ):
    
    if os.path.exists(inputFileXML):
            
            tree = ET.parse(inputFileXML)
            root = tree.getroot()
            words = []
            for speechSegment in root.iter('SpeechSegment'):
                if 'eng-usa' ==  speechSegment.get('lang'):
                    for word in speechSegment.iter('Word'):
                        words.append(word.text[:-1][1:])
            
            audio = root.find('Audio')
            elemtemt = ET.Element('matrix')
            elemtemt.text = str(MeanVectorCalculator(words,wv))
            audio.insert(0,elemtemt)
            #audio.set('matrix', str(matrix / cpt))
            #print(str(matrix / cpt))
            tree.write(inputFileXML)
         
    else:
        print("File " + inputFileXML + " doesn't exist !")
        return -1


        
    
    

class Worker(Thread):
    def __init__(self, FileAdr):
        Thread.__init__(self)
        self.File = FileAdr

    def run(self):
        """Code à exécuter pendant l'exécution du thread."""
        addvectors(self.File)

def addVectorMultipleFiles(listeOfVideo):
    print('begin')
    nb_Threads_max = 200
    if os.path.exists(listOfVideoNames):
        try:
            f = open(listOfVideoNames, 'r')
            file_names = f.read().splitlines()
            f.close()
        except IOError as e:
            print("Could not read file:", listOfVideoNames)
            print(str(e))
            sys.exit()
    else:
        print("File  " + str(listOfVideoNames) + " doesn't exist")
        sys.exit()
    
    threads = []
    cpt = 0
    totFini = 0
    for line in file_names:
        cpt += 1
        if (cpt > nb_Threads_max):
            print(' 200 process already, waiting...')
            for t in threads:
                t.join()
                threads.remove(t)
                totFini += 1
                cpt -= 1
            print(str(totFini) + ' files have been modified' )
     
        w = Worker(str(paths.__PATH_TO_DEV_DESCRIPTORS_L0__ / line))
        w.start()
        threads.append(w)
    
    print(" last files launch")
    for t in threads:
        t.join()
    print("succesfull end of addVectorMultipleFiles")

        
        
    
    
if __name__ == '__main__':
    
    if os.path.exists(listOfVideoNames):
        try:
            f = open(listOfVideoNames, 'r')
            file_names = f.read().splitlines()
            f.close()
        except IOError as e:
            print("Could not read file:", listOfVideoNames)
            print(str(e))
            sys.exit()
    else:
        print("File  " + str(listOfVideoNames) + " doesn't exist")
        sys.exit()
    
    if os.path.exists(str(stock / "learningData.txt")):
        os.remove(str(stock / "learningData.txt"))
    
    for line in file_names:
        print("extraction of " + line)        
        inputFileXML = paths.__PATH_TO_DEV_DESCRIPTORS_L0__ / line        
       
        extractTextENGL(str(inputFileXML))
        
    print("Extraction terminée")
    generateWord2vectorconv()
    """
    addVectorMultipleFiles(listOfVideoNames)
    for line in file_names:
        print("extraction of " + line)        
        inputFileXML = paths.__PATH_TO_DEV_DESCRIPTORS_L0__ / line
        addvectors(str(inputFileXML))
    """

    
    