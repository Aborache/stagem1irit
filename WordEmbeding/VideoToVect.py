# -*- coding: utf-8 -*-
"""
Created on Wed May 29 10:12:28 2019

@author: Poquillon T420s
"""
from gensim.models.doc2vec import Doc2Vec ,TaggedDocument
from datetime import datetime
import numpy as np

tmpFileDocVect = 'tpmDocVec.model'

def buildModel(docs,vector_size=50,save = True):
    model = Doc2Vec(docs, vector_size=vector_size, window=50, min_count=1, workers=4)
    print(datetime.now(), " model builded" )
    if save:
        model.save(tmpFileDocVect)
    return model

def videosContext(nameAndWords,vector_size,recompute = True ,saveModel = True):
    docs = documentFormat(nameAndWords)
    if recompute :
        m = buildModel(docs,vector_size,saveModel)
    else:
        m = Doc2Vec.load(tmpFileDocVect)
    namesAndContexts = {}
    print(datetime.now(), "prepocessing end " )
    for v1 in nameAndWords.keys():
       namesAndContexts[v1] = m.infer_vector(nameAndWords[v1])
    print(datetime.now(), "context generation end " )
    
    return(namesAndContexts,m.docvecs )
    
def documentFormat(nameAndWords):
    documents = [TaggedDocument(' '.join(doc), [i]) for i, doc in nameAndWords.items()]
    return documents

def distanceMatrixComputation(namesAndContexts,docvecs):
    
    videosnames = list(namesAndContexts.keys())
    mSize = len(namesAndContexts)    
    
    dmatrix = np.zeros((mSize,mSize),dtype = float) 
    result = [ np.linalg.norm(namesAndContexts[videosnames[i]] - namesAndContexts[videosnames[j]]) for i in range(0,mSize) for j in range(0 ,i) ]
    cpt = 0
    for i in range(0,mSize):
        dmatrix[i][i]=0
        for j in range (0,i):
            dmatrix[i][j] = result[cpt]
            dmatrix[j][i] = result[cpt]
            cpt += 1    
    return dmatrix


def CorpusBuilder(nameAndWords):
    corpus = []
    for k, v in nameAndWords.items():
        s = ''
        for w in v:
            s += w + ' '
        corpus.append(s)
    return corpus 