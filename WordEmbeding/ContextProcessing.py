# -*- coding: utf-8 -*-
"""
Created on Mon May 27 10:30:42 2019

@author: Lenovo T420s
"""


import numpy as np

from sklearn.cluster import AgglomerativeClustering

import math


from datetime import datetime
vector_dim = 200

def distanceBtwVideos(matrix1,matrix2):
    #d = np.linalg.norm([ np.linalg.norm(e-r) for e in matrix1 for r in matrix2],axis = 1)
    d =np.array( [ np.linalg.norm(matrix1[e]- matrix2[r])* (1 + abs(e-r)) for e in range(0, len(matrix1)) for r in range(0, len(matrix2))])
    d = d.reshape((len(matrix1),len(matrix2)))
    return np.sum(np.min(d,axis = 1) * np.array([1/2 ** i for i in range(1,1 + len(matrix1))]))

def distanceMatrixComputation(matrix):
    mSize = len(matrix)
    dmatrix = np.zeros((mSize,mSize),dtype = float) 
    result = [ distanceBtwVideos(matrix[i],matrix[j]) + distanceBtwVideos(matrix[j],matrix[i]) for i in range(0,mSize) for j in range(0 ,i)]
    cpt = 0
    for i in range(0,mSize):
        dmatrix[i][i]=0
        for j in range (0,i):
            dmatrix[i][j] = result[cpt]
            dmatrix[j][i] = result[cpt]
            cpt += 1    
    return dmatrix
"""
def CorpusBuilder(nameAndWords):
    corpus = []
    for k, v in nameAndWords.items():
        s = ''
        for w in v:
            s += w + ' '
        corpus.append(s)
    return corpus 

def weightsBuilder(nameAndWords) :
    vectorizer = TfidfVectorizer()
    X = vectorizer.fit_transform(CorpusBuilder(nameAndWords))
    nameAndWordWeight =  {}
    cpt = 0
    wordList = vectorizer.get_feature_names()
    for k, v in nameAndWords.items():
        wg = {}
        #totalW = 0
        for w in v:
            try:
                c =  X[cpt,wordList.index(w)]
                if c > 0.2:
                    wg[w] = c
                else :
                    wg[w] = 0
                #wg[w] = X[cpt,wordList.index(w)] ** 3
                #totalW += X[cpt,wordList.index(w)]
            except ValueError :
                wg[w] = 0 
            
        nameAndWordWeight[k] = wg        
        cpt += 1     
    return(nameAndWordWeight)
"""
    

def videoContextComputation(words,vectorisor, wordsWeight, nbContext):
    newWords = set(vectorisor.index2entity).intersection(words)
    
    if len(newWords) < 1:
        return np.zeros((1,vector_dim))

    matrix =  np.array(vectorisor[newWords])
    nbRow = matrix.shape[0] 
    
    if (nbRow < nbContext) or (nbRow == 1):
        return matrix
    newWordsWeight =(np.array( [wordsWeight[w] for w in newWords])).reshape((len(newWords),1))

    context = [] 
    n_clusters = max(int(math.sqrt(len(newWords))), nbContext)

    totalWeight = np.zeros(n_clusters)
    #totalNB = np.zeros(n_clusters)
    totalVect = np.zeros((n_clusters,vector_dim))
    clustering = AgglomerativeClustering(n_clusters, linkage="ward")
    clustering.fit(matrix)
    clusters = clustering.labels_
    for i in range(0,nbRow):
        totalWeight[clusters[i]]+= newWordsWeight[i]
        """
        totalVect[clusters[i]] += matrix[i]
        totalNB[clusters[i]] += 1
        """
        totalVect[clusters[i]] += matrix[i] * newWordsWeight[i]

    sortArgs = np.argsort(totalWeight)[n_clusters-nbContext:]
    for i in range(nbContext -1,-1,-1):
        #context[i] = totalVect[sortArgs[i]] / totalNB[sortArgs[i]]
        if totalWeight[sortArgs[i]] > 0:
            context.append(totalVect[sortArgs[i]] / totalWeight[sortArgs[i]])
        
    return context
   
    

def videosContext(nameAndWords , vectorisor, namesWordWeight,  nbContext,):
    print(datetime.now(), "videosContext begin " )
    namesAndContexts = {}
    namesAndCompat = {}
    print(datetime.now(), "prepocessing end " )
    for v1 in nameAndWords.keys():
        context = videoContextComputation(nameAndWords[v1],vectorisor, namesWordWeight[v1], nbContext)       
        namesAndContexts[v1] = context
        namesAndCompat[v1] = (len(context) >= 1)
        """
        for i in namesAndContexts[v1]:
            print(vectorisor.similar_by_vector(i,1))
        """
    return namesAndContexts,namesAndCompat

def ContextToWords(context, vectorisor):
    return [vectorisor.similar_by_vector(i,1)[0][0] for i in context ]
