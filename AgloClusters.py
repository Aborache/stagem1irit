# -*- coding: utf-8 -*-
"""
Created on Thu Jun 13 10:39:12 2019

@author: Lenovo T420s
"""

from sklearn.cluster import KMeans
from sklearn.cluster import AgglomerativeClustering

import json


from datetime import datetime

import generateWordEmbeding as WE
from Resources import paths
import TreeTools as TT
import numpy as np



"""build the final nodes list"""
def leavesDict(inversDictName,nameAndTags,NameAndContextWords,vect ):
    leaves = {}
    for c,v  in inversDictName.items():
        obj = TT.convert_to_read_([v],nameAndTags,NameAndContextWords)
        obj['vectors']= [vect[v]]
        obj["quantity"] = 1
        leaves[c]= obj
    return leaves
"""from the leaves list and the nodes build a tree close to the one form using ClustersMultiple.py"""
def nodeTree(leavesDict,numberMax,childs,nameAndTags,NameAndContextWords):
    nbleaves = len(leavesDict)
    nodeId = nbleaves
    obj = {}
    while nodeId < numberMax:
        n1 = childs[nodeId - nbleaves][0]
        n2 = childs[nodeId - nbleaves][1]
        child1 = leavesDict[n1]
        child2 = leavesDict[n2]
        
        vids = []
        vids.extend(child1['videos'])
        vids.extend(child2['videos'])
        obj = TT.convert_to_read_(vids,nameAndTags,NameAndContextWords)
        
        vectors = []
        vectors.extend(child1['vectors'])
        vectors.extend(child2['vectors'])
        
        obj['vectors'] = vectors        
        labels = [0] * len(child1['vectors']) + [1] * len(child2['vectors'])
        total,inter,intra = TT.inertia(np.array(vectors),labels,2)
        
        del child1['vectors']
        del child2['vectors']
        del leavesDict[n1 ]
        del leavesDict[n2 ]

        obj["children"]=[ child1 ,child2]
        obj["quantity"] = child1['quantity'] + child2['quantity']
        obj['inertia'] = {'total':total,'inter':inter,'intra':intra}

        
        leavesDict[nodeId] = obj
        nodeId += 1
    return[leavesDict]    
            



"""compute a hierarchical clustering with the vector from the text audio and video"""
def hierarchycalTreeMeanDirect(united, nbCluster= 5,linkage_methode = 'average',kmeansContext =False):
    
    c,mc,t,w,ws,n_v,n_a,n_f = TT.separator(united)
    
    matrix=[]
    vid_matrix = []
    aud_matrix = []
    invertDict = {}
    cpt = 0
    vectors = {}
    for v1,context in c.items():
        if n_f[v1]:
            if kmeansContext:
                matrix.append(mc[v1][0])
                vectors[v1] = mc[v1][0]
            else:
                matrix.append(context)  
                vectors[v1] = context
            
            vid_matrix.append(n_v[v1])
            aud_matrix.append(n_a[v1])
            
            invertDict[cpt]= v1
            cpt+= 1
    print(cpt)        
    vid_matrix = TT.normalization(vid_matrix)
    aud_matrix = TT.normalization(aud_matrix)
    
    dist_audio = TT.DistanceNormalization( TT.DistanceMatrix(aud_matrix))
    dist_video = TT.DistanceNormalization( TT.DistanceMatrix(vid_matrix))
    dist_trans = TT.DistanceMatrix(np.array(matrix))
    
    audio_w = 1
    video_w = 1
    trans_w = 1
    final_dist = dist_audio * audio_w + dist_video * video_w + dist_trans * trans_w
    print(datetime.now(), "data prepared " )   
    a = AgglomerativeClustering(n_clusters = nbCluster,affinity = 'precomputed' ,linkage = linkage_methode )
    
    print(datetime.now(), "processing" )
    a.fit(final_dist)
 
    
    print(datetime.now(), "writing the results" )
    
    root = cpt * 2 - nbCluster
    nodes = a.children_
    l = leavesDict(invertDict,t,ws,vectors )
    print(len(l),len(nodes))
    t = nodeTree(l,root,nodes,t,ws)

    name = 'hirTree-' + str(cpt) + '-' + linkage_methode +'-' + str(nbCluster)

    name += '.json'
    saveFileTxT =  str(WE.stock / name)
    f = open(str(saveFileTxT),'w')
    json.dump(t,f, sort_keys=True, indent=4, separators=(',', ': '))

def demonstrateur():
    print(datetime.now(), "start " )
    united = TT.load_file()
    
    print(datetime.now(), "data load " )

    hierarchycalTreeMeanDirect(united,nbCluster= 10,linkage_methode = "average" )
    
if __name__ == '__main__':
    #glove_file = datapath(str(vectorisorAdrGloveTwitter))    
    #tmp_file = get_tmpfile(str(tmpFile))
    #_ = glove2word2vec(glove_file, tmp_file)
    demonstrateur()    