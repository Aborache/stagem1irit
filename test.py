'''
Created on Jun 21, 2017

@author: TOSHIBA
'''
import os
from .FeaturesL0.extractMetaDataModule import extractMetaData
from .FeaturesL0.extractAudioDataModule import extractAudioData
from .FeaturesL0.extractVideoDataModule import extractVideoData
from .FeaturesL0Combined.combineLowLevelFeatures import generatePercentageDescriptors
from .FeaturesL0.FaceDetection.DetectNumberOfFaces import extractFaceFromKeyframes
from .FeaturesL0.PMBSegmentation_Julien.PMBFunction import PMBSegmentation
from .FeaturesL0Combined.generateTypeSpeakers import classifiySpeakers
from .Resources import paths
import xml.dom.minidom
import io
import traceback

def loadPMBSegmentationFromOldFiles(oldPath, newPath, fileName):
    f = open("toDoCompletely.txt","a")
    try:
        DOMTreeIn = xml.dom.minidom.parse(oldPath)
        DOMTreeOut = xml.dom.minidom.parse(newPath)
        rootIn = DOMTreeIn.documentElement
        rootOut = DOMTreeOut.documentElement
    
        audio = DOMTreeOut.getElementsByTagName("Audio")[0]
        pseg = DOMTreeIn.getElementsByTagName("SpeechSegmentation")
        mseg = DOMTreeIn.getElementsByTagName("MusicSegmentation")
        if len(pseg)==0 or len(mseg)==0:
            f.write(fileName+"\n")
            f.close()
            return -1
        audio.appendChild(pseg[0])
        audio.appendChild(mseg[0])
    
        file_handle = io.open(newPath, 'wb')
        rootOut.writexml(file_handle)
        file_handle.close()
        f.close()
        return 0
        #print ('ASR file opened for processing: '+ asrFilePath)
    except IOError:
        f.write(fileName+"\n")
        print ('loading PMB segmentation from old file failed')
        print (oldPath)
        f.close()
        return -1

def generate(nameFileVideo):
    
    nameFileOutputXML = paths.__PATH_TO_DEV_DESCRIPTORS_L0__ + nameFileVideo[:len(nameFileVideo)-8] + ".xml"
    nameFileVideoPath = paths.__PATH_TO_DEV_VIDEO__ + nameFileVideo
    nameFileAudioPath = paths.__PATH_TO_DEV_AUDIO__ + "6-2017\\" + nameFileVideo[:len(nameFileVideo)-8] + ".wav"
    
    metadataFilePath = paths.__PATH_TO_DEV_METADATA__ + nameFileVideo[:len(nameFileVideo)-8] + ".xml"
    videoFilePath = paths.__PATH_TO_DEV_SHOTS__ + nameFileVideo[:len(nameFileVideo)-8] + ".xml"
    asrFilePath = paths.__PATH_TO_DEV_ASR__ + nameFileVideo[:len(nameFileVideo)] + ".xml"
     
    #vmeta = extractMetaData(metadataFilePath,nameFileOutputXML)
    #print vmeta
#     try:
#         
#         vaudio = extractAudioData(asrFilePath,nameFileOutputXML)
#         print vaudio
#         
#     except:
#         traceback.print_exc()
    
    #vvideo = extractVideoData(videoFilePath,nameFileOutputXML, nameFileVideoPath)
    #print vvideo
    
    #v = vseg = PMBSegmentation(['--4Hz','--NBS', '-i', nameFileAudioPath],nameFileOutputXML)
    #print v
    
    
    
    
    
    
    
    #vframes = extractFaceFromKeyframes(generalPath + "\\" + part + "SHOT\\", nameFileOutputXML, nameFileVideoPath)
    #print vframes
    
    generatePercentageDescriptors(paths.__PATH_TO_DEV_DESCRIPTORS_L0__ + nameFileVideo[:len(nameFileVideo)-8] + ".xml",
                                 "/home/hassan/6-2017/l1- only one level/" + nameFileVideo[:len(nameFileVideo)-8] + ".xml", 1)
    
if __name__ == '__main__':
    #generate("Aabbey1-DavidHartmanTheContingentFeatureOfTheJewishViewOfHisto566.flv.ogv")
    
    with open(paths.__FINAL_DESCRIPTORS_LIST_FLV_OGV__, 'r') as f:
        allLines = f.read().splitlines()
        for line in allLines:
            try:
                print(line)
                generate(line[:-4] + ".flv.ogv")
            except :
                pass
    